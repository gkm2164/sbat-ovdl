#include <cstdlib>

#include <iostream>

#include <sbat.console.hh>
#include <sbat.builtin.hh>
#include <statement/sbat.varname.hh>
#include <sbat.symboltable.hh>

#include <memory.h>
#include <sys/stat.h>

namespace SBAT {
  /* Input: filename without extension */
  bool is_modified(string filename) {
    string src = filename + ".c";
    string obj = filename + ".o";
    const char *psrc = src.c_str ();
    const char *pobj = obj.c_str ();

    struct stat fst_src, fst_obj;
    memset (&fst_src, 0x00, sizeof(struct stat));
    memset (&fst_obj, 0x00, sizeof(struct stat));

    if (stat(psrc, &fst_src) != 0) {
      msg_prn (msg_type::ERROR, msg_context::START, "File [" + src + "] does not exist!", true);
      exit (-1);
    } if (stat (pobj, &fst_obj) == 0) {
      if (fst_src.st_mtime < fst_obj.st_mtime)  {
        return false;
      }
    }
    return true;
  }

  void builtin_compile(std::vector<Expression *> *arglist, void *ptr) {
    extern char **environ;
    
    VarNameExp *ve = (VarNameExp *)(*arglist)[0];
    string name = ve->getName();
    string refname = name + ".src";
    
    Variable *fname = (Variable *)current_stable->findByName (name + ".name", symbol_type::STRING);
    Variable *defstr = (Variable *)current_stable->findByName ("current_binding.defstr", symbol_type::STRING);
    Variable *var = (Variable *)current_stable->findByName (refname, symbol_type::STRING);

//    if (is_modified(fname->getStrValue())) {
      msg_prn (msg_type::INFO, msg_context::START, "Compile " + var->getStrValue(), true);
      string command = "gcc -c " + var->getStrValue() + " " + defstr->getStrValue();
      system (command.c_str());
//    }
    
    /* Hello World */
  }
}
