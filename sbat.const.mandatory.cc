#include <vector>

using namespace std;

#include <sbat.defines.hh>
#include <sbat.const.mandatory.hh>
#include <sbat.constraint.hh>
#include <sbat.fragment.hh>
#include <sbat.binding.hh>

typedef vector<SBAT::ABind *> abind_vector;

namespace SBAT {
  Mandatory::Mandatory(int opcode, vector<Argument *> *flist)
    :Constraint (opcode, flist) {
  }
  
  bool
  Mandatory::evaluate(Binding *bind) {
    int mask = arg_type::ARG_VNAME |
               arg_type::ARG_VPNAME |
               arg_type::ARG_REFERENCE;

    vector<Argument *> *ops = getOperand();
    Argument *_op1 = (*ops)[0];

    string op1 = _op1->getSValue();

    abind_vector *avect = bind->getBindList();
    abind_vector::const_iterator ait = avect->begin();

    bool op1_exist = false;

    foreach(abind_vector, ait, avect) {
      ABind *elem = *ait;
      
      if (op1 == elem->second)
        op1_exist = true;
    }

    if (op1_exist) {
      return true;
    }

    return false;
  }
}
