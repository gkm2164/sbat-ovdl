#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include <sbat.platform.hh>

namespace SBAT {
  extern vector<string> all_variants;

  Platform::Platform(vector<string> *_elems):
  	elems(_elems) {
    for (int i = 0; i < elems->size(); i++) {
      string elemname = (*elems)[i];
      if (elemname.substr(0, 2) != "vp") {
        all_variants.push_back(elemname);
      }
    }
  }

	vector<string>::const_iterator
	Platform::getIterator() {
		return elems->begin();
	}

	vector<string>::const_iterator
	Platform::getEnd() {
		return elems->end();
	}
}
