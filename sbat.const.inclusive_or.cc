#include <iostream>
#include <vector>
#include <sstream>

using namespace std;

#include <cstdlib>

#include <sbat.defines.hh>

#include <sbat.constraint.hh>
#include <sbat.const.inclusive_or.hh>
#include <sbat.fragment.hh>
#include <sbat.binding.hh>
#include <sbat.symboltable.hh>
#include <sbat.symboltable.type.hh>
#include <sbat.console.hh>

typedef vector<SBAT::ABind *> abind_vector;
typedef SBAT::symbol_type stype;

namespace SBAT {
  InclusiveOR::InclusiveOR(int opcode, vector<Argument *> *flist)
    :Constraint (opcode, flist) {
  }

  bool
  InclusiveOR::evaluate(Binding *bind) {
    int mask_vp = arg_type::ARG_VPNAME;
    int mask_num = arg_type::ARG_NUMBER;
    vector<Argument *> *ops = getOperand();

    msg_prn (msg_type::INFO, msg_context::START, "Inclusive OR constraint test", true);
    Argument *_op1 = (*ops)[0];
    Argument *_op2 = (*ops)[1];
    Argument *_op3 = (*ops)[2];

    if (_op1->getType() != mask_vp ||
        _op2->getType() != mask_num ||
        _op2->getType() != mask_num) {
      msg_prn (msg_type::ERROR, msg_context::START, 
        "Argument is not correct", true);
      return false;
    }

    int varcnt = 0;

    string vpname = _op1->getSValue();
    int min = _op2->getIValue();
    int max = _op3->getIValue();

    stringstream minss, maxss;
    string minstr, maxstr;
    minss << min;
    maxss << max;
    minstr = minss.str();
    maxstr = maxss.str();

    msg_prn (msg_type::INFO, msg_context::START, "Check constraint for " + vpname + " where " + minstr + " <= X <= " + maxstr, true);
    abind_vector *avect = bind->getBindList();
    if (avect == NULL) {
      if (min && max) {
        msg_prn (msg_type::ERROR, msg_context::START, "Inclusive OR constraint is not kept", true);
        return false;
      }
      msg_prn (msg_type::INFO, msg_context::START,
        "Message info is correct", true);
      return true;
    }

    foreach(abind_vector, ait, avect) {
      ABind *elem = *ait;
      string refName = elem->first + "." + elem->second;

      Variant *v = (Variant *)current_stable->findByName(refName, stype::VARIANT);
      if (v == NULL) {
        msg_prn (msg_type::ERROR, msg_context::START, "There's no such variant: " + refName, true);
        exit(-1);
      }
      VariablePoint *vp = v->getParent();

      if (vp->getName() == vpname) {
        varcnt++;
      }
    }

    return varcnt >= min && varcnt <= max;
  }
}
