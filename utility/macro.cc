#include <utility/macro.hh>

using namespace std;

Macro::Macro(MacroTable *_macro_tbl, string _source):
  macro_tbl(_macro_tbl), source(_source) {
}

int
Macro::replaceString(int pos) {
  int rstart = pos,
      rend = pos;

  while (rend < source.size()) {
    if (source[rend] == '@') {
      break;
    }
    rend++;
  }

  string repstr = source.substr(rstart, rend - rstart);
  MacroTable::const_iterator ptr = macro_tbl->find(repstr);
  if (ptr == macro_tbl->end()) {
    sout << "@!!Unknown Variable = " << repstr << "@";
    return rend;
  }

  sout << ptr->second;
  return rend;
}

string
Macro::replaceString() {
  bool inreplace = false;
  for (int i = 0; i < source.size(); i++) {
    if (inreplace) {
      i = replaceString(i);
      inreplace = false;
    } else if (source[i] == '@') {
      inreplace = true;
      continue;
    } else {
      sout << source[i];
    }
  }

  return sout.str();
}

string
Macro::toConfigFile(string filename) {
  string output = "#ifndef _CONFIG_FILE\n#define _CONFIG_FILE\n";
  MacroTable::const_iterator ptr = macro_tbl->begin();
  while(ptr != macro_tbl->end()) {
    string key = ptr->first;
    string value = ptr->second;

    output = "#define " + key + "\t" + value + "\n";
  }
  output += "#endif";
  return output;
}
