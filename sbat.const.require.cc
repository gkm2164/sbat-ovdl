#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include <sbat.console.hh>
#include <sbat.defines.hh>
#include <argument/sbat.argument.hh>
#include <argument/sbat.arg.vname.hh>
#include <argument/sbat.arg.vpname.hh>
#include <argument/sbat.arg.reference.hh>
#include <sbat.const.require.hh>
#include <sbat.fragment.hh>
#include <sbat.constraint.hh>
#include <sbat.binding.hh>

typedef vector<SBAT::ABind *> abind_vector;

namespace SBAT {
  Require::Require(int opcode, vector<Argument *> *flist)
    :Constraint (opcode, flist) {
  }
  
  bool
  Require::evaluate(Binding *bind) {
    int mask = arg_type::ARG_VNAME | arg_type::ARG_VPNAME | arg_type::ARG_REFERENCE;
    vector<Argument *> *ops = getOperand();
    Argument *_op1 = (*ops)[0];
    Argument *_op2 = (*ops)[1];

    msg_prn(msg_type::INFO, msg_context::START, "Check constraint [require] for " + bind->getName(), true);

    if (!(_op1->getType() & mask)) {
      msg_prn(msg_type::DEBUG, msg_context::START, "First operand is not variant name", true);
    }

    if (!(_op2->getType() & mask)) {
      msg_prn(msg_type::DEBUG, msg_context::START, "Second operand is not variant name", true);
    }

    string op1 = _op1->getSValue(),
           op2 = _op2->getSValue();

    vector<string> *avect = bind->getBinds();
  
    bool op1_exist = false,
         op2_exist = false;
  
    foreach(vector<string>, ait, avect) {
      string elem = *ait;

      if (op1 == elem) { op1_exist = true; }
      if (op2 == elem) { op2_exist = true; }
    }

    if (op1_exist && !op2_exist) {
      msg_prn(msg_type::ERROR, msg_context::START, "Element " + op1 + " exist but " + op2 + " is not.", true);
      return false;
    }
    return true;
  }
}
