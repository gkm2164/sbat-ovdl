#include <iostream>
#include <vector>

using namespace std;

#include <sbat.constraint.hh>
#include <sbat.fragment.hh>

#include <sbat.const.mandatory.hh>
#include <sbat.const.optional.hh>
#include <sbat.const.require.hh>
#include <sbat.const.exclude.hh>
#include <sbat.const.inclusive_or.hh>
#include <sbat.const.exclusive_or.hh>

namespace SBAT {
  
  Constraint::Constraint (int opcode, vector<Argument *> *ops) {
    this->opcode = opcode;
    this->operands = ops;
  }

  bool
  Constraint::evaluate(Binding *binding) { return false; }

  Constraint *
  Constraint::newConstraint(int opcode, vector<Argument *> *flist) {
    Constraint *ret = NULL;

    switch (opcode) {
      case MANDATORY:
        ret = new Mandatory(opcode, flist);
        break;
      case OPTIONAL:
        ret = new Optional(opcode, flist);
        break;
      case REQUIRE:
        ret = new Require(opcode, flist);
        break;
      case EXCLUDE:
        ret = new Exclude(opcode, flist);
        break;
      case INCLUSIVE_OR:
        ret = new InclusiveOR(opcode, flist);
        break;
      case EXCLUSIVE_OR:
        ret = new ExclusiveOR(opcode, flist);
        break;
      default:
        return NULL;
        break;
    }

    return ret;
  }

}
