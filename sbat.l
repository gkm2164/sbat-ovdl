%option noyywrap
%x SCMD STRSTATE

%{

#include <sbat.symboltable.hh>
#include "sbat.tab.hh"


#define YY_DECL \
  int SBATlex(SBAT::Parser::semantic_type *yylval, \
              SBAT::Parser::location_type *yylloc)

typedef SBAT::Parser::token token;
typedef SBAT::Expression::compare compare;

#define yyterminate() return token::END

#define YY_USER_ACTION yylloc->columns(yyleng);
%}

WORD ([a-zA-Z_][a-zA-Z0-9_]*)

%%
%{
  yylloc->step();
%}


\!\{ { yylval->sval = new string(""); BEGIN SCMD; }
<SCMD>[ \t]  { *yylval->sval += *(new string(yytext)); yylloc->step (); }
<SCMD>[\n\r] { *yylval->sval += *(new string(yytext)); yylloc->lines(1); }
<SCMD>[^\}]* { *yylval->sval += *(new string(yytext)); return token::SHELLCMD; }
<SCMD>\} { yylloc->step(); BEGIN INITIAL; }

[ \t] { yylloc->step(); }
[\n\r] { yylloc->lines(1); }

[,;:\\\{\}\(\)\[\]] { return yytext[0]; }

CodeMap     { return token::CODEMAP; }
Platform    { return token::PLATFORM; }
VPDec       { return token::VPDEC; }
Constraints { return token::CONSTRAINTS; }
Binding     { return token::BINDING; }
WorkDef     { return token::WORKDEF; }

depend { return token::DEPEND; }
on { return token::ON; }

mandatory { yylval->opcode = MANDATORY; return token::CONST_OPERATION; }
optional  { yylval->opcode = OPTIONAL; return token::CONST_OPERATION; }
require   { yylval->opcode = REQUIRE; return token::CONST_OPERATION; }
exclude   { yylval->opcode = EXCLUDE; return token::CONST_OPERATION; }
inclusive_or { yylval->opcode = INCLUSIVE_OR; return token::CONST_OPERATION; }
exclusive_or { yylval->opcode = EXCLUSIVE_OR; return token::CONST_OPERATION; }

if { return token::IF; }
then { return token::THEN; }
else { return token::ELSE; }
elseif { return token::ELSEIF; }
for { return token::FOR; }
do { return token::DO; }
while { return token::WHILE; }
endif { return token::ENDIF; }
endfor { return token::ENDFOR; }
endwhile { return token::ENDWHILE; }
shell { return token::SHELL; }

"<" { yylval->cmptype = compare::LT; return token::CMP; }
">" { yylval->cmptype = compare::GT; return token::CMP; }
"==" { yylval->cmptype = compare::EQ; return token::CMP; }
"<=" { yylval->cmptype = compare::LE; return token::CMP; }
">=" { yylval->cmptype = compare::GE; return token::CMP; }
"!=" { yylval->cmptype = compare::NE; return token::CMP; }
"&&" { yylval->cmptype = compare::AND; return token::CMP; }
"||" { yylval->cmptype = compare::OR; return token::CMP; }

"!" |
"+" |
"-" |
"*" |
"/" |
"=" { return yytext[0]; }

\" { yylval->sval = NULL; BEGIN STRSTATE; }
<STRSTATE>[^\"]* { yylval->sval = new std::string (yytext); return token::STRING; }
<STRSTATE>\" { BEGIN INITIAL; }

sub{WORD} {
  yylval->sval = new string(yytext);
  return token::SUBNAME;
}

vp{WORD} {
  yylval->sval = new string(yytext);
  return token::VPNAME;
}

v{WORD} {
  yylval->sval = new string(yytext);
  return token::VNAME;
}

[a-zA-Z_][a-zA-Z0-9_]* {
	yylval->sval = new string(yytext);
	return token::VARIABLE;
}

{WORD}(\.{WORD})+ {
  yylval->sval = new string(yytext);
  return token::REFERENCE;
}

{WORD}(\/{WORD})+ {
  yylval->sval = new string(yytext);
  return token::SUBPLDTREFERENCE;
}

[0-9]+ {
	yylval->ival = atoi(yytext);
	return token::NUMBER;
}

. 

%%
