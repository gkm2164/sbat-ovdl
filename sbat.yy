%language "C++"
%defines
%locations

%name-prefix "SBAT"
%define parser_class_name {Parser}

%code requires {
#include <iostream>
#include <string>
#include <vector>
#include <map>

#include <sbat.hh>

using namespace std;
}

%{
#include <iostream>
#include <string>
#include <vector>
#include <map>

#include <sbat.hh>
#include "sbat.tab.hh"

using namespace std;
using namespace SBAT;


int yylex(SBAT::Parser::semantic_type *yylval,
          SBAT::Parser::location_type *yylloc);

typedef SBAT::symbol_type stype;

#define LIST_DECL(__dst, __type) (__dst) = new std::vector<__type> ()
#define INSERT_BEGIN(__src__, __dst__) (__src__)->insert ((__src__)->begin(), (__dst__))
%}

%union {
  int ival;
  int opcode;

  SBAT::Script *scrdecl;

  std::map<string, string> *cmlval;
  SBAT::CodeMap *cmval;

  SBAT::Platform *pfdecl;
  
  std::vector<SBAT::Fragment *> *flval;
  SBAT::Fragment *fval;

  std::vector<SBAT::VariablePoint *> *vplval;
  SBAT::VariablePoint *vpval;

  std::vector<string> *slval;
  std::string *sval;
  
  std::vector<SBAT::Constraint *> *cstlval;
  SBAT::Constraint *cstval;

  std::vector<SBAT::Binding *> *bdlval;
  SBAT::Binding *bdval;
  std::vector<SBAT::ABind *> *blval;
  SBAT::ABinding *bval;

  std::vector<SBAT::Argument *> *alval;
  SBAT::Argument *aval;

  std::vector<SBAT::WorkDef *> *wlval;
  SBAT::WorkDef *wval;

  std::vector<SBAT::Statement *> *stlval;
  SBAT::Statement *stval;
  std::vector<SBAT::IFStatement *> *iflval;
  SBAT::IFStatement *ifval;
  SBAT::FORStatement *forval;
	SBAT::WHILEStatement *whileval;
	std::vector<SBAT::Expression *> *elval;
	SBAT::Expression *eval;
  SBAT::SymbolExp *symval;
  SBAT::TermExp *tval;
	
  SBAT::SHELLStatement *shval;

  int cmptype;
}

%token END
%token CODEMAP PLATFORM VPDEC CONSTRAINTS BINDING WORKDEF
%token DEPEND ON

%token<ival> NUMBER
%token<sval> STRING
%token<sval> SUBNAME VARIABLE VNAME VPNAME REFERENCE SUBPLDTREFERENCE
%token<opcode> CONST_OPERATION

%type<scrdecl> script

%type<pfdecl> platform
%type<slval> fragments
%type<sval> fragment

%type<vplval> vpdecpart vpdecs
%type<vpval> vpdec

%type<slval> vnamelist vnames

%type<slval> variants vars

%type<cstlval> constpart consts
%type<cstval> const

%type<bdlval> bindingpart binddecls
%type<bdval> binddecl
%type<blval> binds_vect binds
%type<bval> bind

%type<alval> args
%type<aval> arg

%type<wlval> workdefpart
%type<wval> workdef

%type<slval> forwhats
%type<sval> forwhat

%type<stlval> stmts elsestmt
%type<stval> stmt

%type<iflval> elseifstmts
%type<ifval> ifstmt elseifstmt
%type<forval> forstmt
%type<whileval> whilestmt

%type<elval> exps
%type<eval> exp

%type<symval> symbol
%type<tval> term

%type<shval> shell
%token<sval> SHELLCMD

%token IF THEN ELSEIF ELSE FOR DO WHILE ENDIF ENDFOR ENDWHILE SHELL

%token<cmptype> CMP

%nonassoc CMP
%right '='
%left '+' '-'
%left '*' '/'
%nonassoc UNOT UMINUS

%start script
%%
 
script: platform vpdecpart constpart bindingpart workdefpart {
 		$$ = new SBAT::Script($1, $2, $3, $4, $5);
    
  	if ($$->applyBinding() != true) {
      cout << "Failed to applying binding!" << endl;
      YYABORT;
    }

    $$->runScript();
  	YYACCEPT;
	}
;

platform: PLATFORM ':' '{' fragments '}' { $$ = new SBAT::Platform($4); }
;

fragments: fragment { LIST_DECL($$, string); INSERT_BEGIN($$, *$1); }
| fragment ',' fragments { $$ = $3; INSERT_BEGIN($$, *$1); }
;

fragment: VNAME {
		$$ = $1;
		Variant *x = new Variant(*$1, true);
		current_stable->insertNew(*$1, stype::VARIANT, x);
	}
| VPNAME {
		$$ = $1;
		VariablePoint *vp = new VariablePoint(*$1);
		current_stable->insertNew(*$1, stype::VARIABLE_POINT, vp);
	}
;

vpdecpart: /* epsilon */ { $$ = NULL; }
| VPDEC ':' '{' vpdecs '}' { $$ = $4; }
;

vpdecs: vpdec { LIST_DECL($$, VariablePoint *); INSERT_BEGIN($$, $1); }
| vpdec ',' vpdecs { $$ = $3; INSERT_BEGIN($$, $1); }
;

vpdec: VPNAME ':' '{' variants '}' {
    VariablePoint *vp = (VariablePoint *)current_stable->findByName(*$1, stype::VARIABLE_POINT);

    if (vp == NULL) {
      cout << "SBAT >> There's no variation point like " << *$1 << endl;
      YYABORT;
    }

    $$ = vp;
    $$->addVariants($4);
  }
;

variants: VNAME { LIST_DECL($$, string); INSERT_BEGIN($$, *$1); }
| VNAME ',' variants { $$ = $3; INSERT_BEGIN($$, *$1); }
;

constpart: /* epsilon */ { $$ = NULL; }
| CONSTRAINTS ':' '{' consts '}' { $$ = $4; }
;

consts: const { LIST_DECL($$, Constraint *); INSERT_BEGIN($$, $1); }
| const ',' consts { $$ = $3; INSERT_BEGIN($$, $1); }
;

const: CONST_OPERATION '(' args ')' {
		$$ = Constraint::newConstraint($1, $3);
	}
;

args: arg { LIST_DECL($$, Argument *); INSERT_BEGIN($$, $1); }
| arg ',' args { $$ = $3; INSERT_BEGIN($$, $1); }
;

vnamelist: '{' vnames '}' { $$ = $2; }
;

vnames: VNAME { LIST_DECL($$, string); INSERT_BEGIN($$, *$1); }
| VNAME ',' vnames { $$ = $3; INSERT_BEGIN($$, *$1);}
;

arg: VNAME { $$ = new ArgVName(*$1); }
| VPNAME { $$ = new ArgVPName(*$1); }
| VARIABLE { $$ = new ArgVariableName(*$1); }
| REFERENCE { $$ = new ArgReference(*$1); }
| NUMBER { $$ = new ArgNumber($1); }
| STRING { $$ = new ArgString(*$1); }
| vnamelist { $$ = new ArgVNameList($1); }
;

bindingpart: BINDING ':' '{' binddecls '}' {
		$$ = $4;
	}
;

binddecls: binddecl { LIST_DECL($$, Binding *); INSERT_BEGIN($$, $1);  }
| binddecl ',' binddecls { $$ = $3; INSERT_BEGIN($$, $1); }
;

binddecl: VARIABLE ':' '[' binds_vect ']' {
		$$ = new Binding(*$1, $4);
		current_stable->insertNew (*$1, stype::BINDING, $$);
	}
;

binds_vect: /* Epsilon */ { $$ = NULL; }
| binds { $$ = $1; }
;

binds: bind {
    LIST_DECL($$, ABind *);
    ABinding::addBinding($$, $1);
  }
| bind ',' binds {
    $$ = $3;
    ABinding::addBinding($$, $1);
  }
;

bind: VPNAME '\\' VNAME {
    string refName = *$1 + "." + *$3;
    Variant *v = (Variant *)current_stable->findByName(refName, stype::VARIANT);
    VariablePoint *vp = (VariablePoint *)current_stable->findByName(*$1, stype::VARIABLE_POINT);

    if (v == NULL) {
      cout << "SBAT >> There's no variant like " << refName << endl;
      YYABORT;
    }
    if (vp == NULL) {
      cout << "SBAT >> There's no variation point like " << *$1 << endl;
      YYABORT;
    }

    $$ = new ABinding(*$1, *$3);
  }
| VPNAME '\\' vnamelist {
    $$ = new ABinding(*$1, $3);
  }
;

workdefpart: workdef {
    LIST_DECL($$, WorkDef *); INSERT_BEGIN($$, $1);
    current_stable->insertNew ($1->getName(), stype::WORK, $1);
  }
| workdef workdefpart {
    $$ = $2; INSERT_BEGIN($$, $1);
    current_stable->insertNew ($1->getName(), stype::WORK, $1);
  }
;

workdef:
  WORKDEF VARIABLE '(' forwhats ')' '{' stmts '}' {
    $$ = new WorkDef(*$2, $4, $7);
  }
| WORKDEF VARIABLE '(' forwhats ')' ':' DEPEND ON '(' vars ')' '{' stmts '}' {
    $$ = new WorkDef(*$2, $4, $13); $$->add_dependency($10);
  }
;

vars: VARIABLE { LIST_DECL($$, string); INSERT_BEGIN($$, *$1); }
| VARIABLE ',' vars { $$ = $3; INSERT_BEGIN($$, *$1); }
;

forwhats: forwhat { LIST_DECL($$, string); INSERT_BEGIN($$, *$1); }
| forwhat ',' forwhats { $$ = $3; INSERT_BEGIN($$, *$1); }
;

forwhat: { }
| VNAME { $$ = $1; }
| VPNAME { $$ = $1; }
| VARIABLE { $$ = $1; }
| REFERENCE { $$ = $1; }
;

stmts: { LIST_DECL ($$, Statement *); }
| stmt stmts { $$ = $2; INSERT_BEGIN($$, $1); }
;

stmt: ifstmt { $$ = $1; }
| forstmt { $$ = $1; }
| whilestmt { $$ = $1; }
| exp ';' { $$ = $1; }
| shell { $$ = $1; }
;

ifstmt: IF '(' exp ')' THEN stmts ENDIF { $$ = new IFStatement ($3, $6); }
| IF '(' exp ')' THEN stmts elsestmt ENDIF { $$ = new IFStatement ($3, $6, $7); }
| IF '(' exp ')' THEN stmts elseifstmts ENDIF { $$ = new IFStatement ($3, $6, $7); }
| IF '(' exp ')' THEN stmts elseifstmts elsestmt ENDIF { $$ = new IFStatement ($3, $6, $7, $8); }
;

elseifstmts: elseifstmt { LIST_DECL($$, IFStatement *); INSERT_BEGIN($$, $1); }
| elseifstmt elseifstmts { $$ = $2; INSERT_BEGIN($$, $1); }
;

elseifstmt: ELSEIF '(' exp ')' THEN stmts { $$ = new IFStatement ($3, $6); }
;

elsestmt: ELSE THEN stmts { $$ = $3; }
;

forstmt: FOR '(' exp ';' exp ';' exp ')' DO stmts ENDFOR { $$ = new FORStatement($3, $5, $7, $10); }
;

whilestmt: WHILE '(' exp ')' DO stmts ENDWHILE { $$ = new WHILEStatement($3, $6); }
;

exp: exp CMP exp { $$ = new BinaryExp($2, $1, $3); }
| exp '+' exp { $$ = new BinaryExp('+', $1, $3); }
| exp '-' exp { $$ = new BinaryExp('-', $1, $3); }
| exp '*' exp { $$ = new BinaryExp('*', $1, $3); }
| exp '/' exp { $$ = new BinaryExp('/', $1, $3); }
| '(' exp ')' { $$ = $2; }
| '!' exp %prec UNOT { $$ = new UnaryExp('N', $2); }
| '-' exp %prec UMINUS { $$ = new UnaryExp('M', $2); }
| term { $$ = $1; }
| symbol { $$ = $1; }
| symbol '=' exp { $$ = new SubstExp($1, $3); }
| symbol '(' exps ')' { $$ = new FuncCallExp($1, $3); }
| symbol '(' ')' { $$ = new FuncCallExp($1, NULL); }
;

symbol: VNAME { $$ = new VNameExp (*$1); }
| VPNAME { $$ = new VPNameExp (*$1); }
| VARIABLE { $$ = new VarNameExp (*$1); }
| REFERENCE { $$ = new ReferenceExp (*$1); }
;

term: NUMBER { $$ = new NumberExp ($1); }
| STRING { $$ = new StringExp (*$1); }
;

exps: exp { LIST_DECL($$, Expression *); INSERT_BEGIN($$, $1); }
| exp ',' exps { $$ = $3; INSERT_BEGIN($$, $1); }
;

shell: SHELL '(' STRING ')' SHELLCMD {
		$$ = new SHELLStatement(*$3, *$5);
	}
;

%%

namespace SBAT {

  void
  Parser::error (SBAT::location const& loc, std::string const& msg) {
    cout << "Error at " << loc << ": " << msg << endl;  
  }
}
