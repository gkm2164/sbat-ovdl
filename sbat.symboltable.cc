#include <cstdio>

#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <stack>
#include <map>

using namespace std;
#include <sbat.console.hh>

#include <sbat.variable.hh>

#include <sbat.symboltable.hh>

#include <utility/macro.hh>

namespace SBAT {
  SymbolTable *current_stable;
  stack<SymbolTable *> stable_stack;

  SymbolTable::SymbolTable ():parent (NULL) {
    frags = new map<string, Symbol *>();
  }

  SymbolTable::SymbolTable (SymbolTable *_par): parent(_par) {
  	frags = new map<string, Symbol *>();
  }

	void
	SymbolTable::insertNew(string name, int type, void *ptr) {
    string type_str[] = {
      "VARIANT",
      "VARIATION_POINT",
      "VARIABLE",
      "BINDING",
      "NUMBER",
      "STRING",
      "WORK",
      "FUNCTION"
    };
		frags->insert(std::pair<string, Symbol *>(name, new Symbol (type, ptr)));
	  //msg_prn(msg_type::DEBUG, "Insert new symbol: " + name + " with " + type_str[type], true);
  }

  void
  SymbolTable::removeIfExist(string name) {
    map<string, Symbol *>::iterator it = frags->find(name);

    if (it == frags->end()) {
      if (parent != NULL) {
        parent->removeIfExist(name);
      }
      return;
    }

    frags->erase(it);
  }

	void *
	SymbolTable::findByName(string name, int type) {
		map<string, Symbol *>::iterator it = frags->find(name);

		if (it == frags->end()) {
			if (parent != NULL) {
				return parent->findByName(name, type);
			} else {
				return NULL;
			}
		}

		Symbol *data = it->second;
		if (data->getType() != type) {
			return NULL;
		}

		return data->getData();
	}

  bool
  SymbolTable::setTrigger(string name, WorkDef *ptr) {
    map<string, Symbol *>::iterator it = frags->find(name);
    msg_prn (msg_type::DEBUG, msg_context::START, "Register trigger " + ptr->getName() + " on " + name, true);
    if (it == frags->end()) {
      return false;
    }
    
    it->second->setTrigger(ptr);

    return true;
  }

  WorkDef *SymbolTable::getTrigger(string name) {
    map<string, Symbol *>::iterator it = frags->find(name);

    if (it == frags->end()) {
      return NULL;
    }

    return (it->second)->getTrigger();
  }

  SymbolTable *
  SymbolTable::getParent() {
  	return parent;
  }

	int
	SymbolTable::getSymbolType(string name) {
		map<string, Symbol *>::iterator it = frags->find(name);

		if (it == frags->end()) {
      if (parent != NULL) {
        return parent->getSymbolType(name);
      } else {
        return -1;
      }
		}

		Symbol *symb = it->second;
		return symb->getType();
	}	

  void
  SymbolTable::showElements() {

    #ifdef DEBUG
    if (parent != NULL) {
      parent->showElements();
    }

		map<string, Symbol *>::iterator it = frags->begin();

		while (it != frags->end()) {
      cout << "Symbol Name: [" << it->first << "] Type - [";
      cout << getSymbolType(it->first) << "]" << endl;

			it++;
		}
    #endif
  }

  void createNewSymbolTable () {
    stable_stack.push(current_stable);
    current_stable = new SymbolTable(current_stable);
  }

  SymbolTable *recoverOldSymbolTable() {
    SymbolTable *ret = current_stable;

    current_stable = stable_stack.top();

    stable_stack.pop();

    return ret;
  }

	/* Transform symbol table's contents to macro table. */
	void
	SymbolTable::toMacroTable(MacroTable &mactbl) {
    extern string EXTENSION_NAME; 

		if (parent) {
      parent->toMacroTable(mactbl);
		}

		map<string, Symbol *>::const_iterator cit = frags->begin();

		while (cit != frags->end()) {
			pair<string, Symbol *> elem = *cit;
			string name = elem.first;
			Symbol *item = elem.second;

			string data = "";
			switch (item->getType()) {
				case symbol_type::VARIANT:
					{
						Variant *v = (Variant *)item->getData();
						data = v->getName();
						mactbl.insert(MacroPair(name, data));
					}
					break;
				case symbol_type::VARIABLE_POINT:
					{
					}
					break;
				case symbol_type::VARIABLE:
					{
            Variable *var = (Variable *)item->getData();
            switch (var->getType()) {
              case VARTYPE::INTEGER:
              {
                stringstream ss;
                ss << var->getIntValue();
                mactbl.insert(MacroPair(name, ss.str()));
                break;
              }
              case VARTYPE::STRING:
              {
                stringstream ss;
                ss << var->getStrValue();
                mactbl.insert(MacroPair(name, ss.str()));
                break;
              }
            }
					}
					break;
				case symbol_type::BINDING:
					{
            Binding *binding = (Binding *)item->getData();
            mactbl.insert(MacroPair(name, binding->getName()));
					}
					break;
				case symbol_type::NUMBER:
					{
						Variable *var = (Variable *)item->getData();
						stringstream ss;
						ss << var->getIntValue();
						mactbl.insert(MacroPair(name, ss.str()));
					}
					break;
				case symbol_type::STRING:
					{
						Variable *var = (Variable *)item->getData();
						mactbl.insert (MacroPair(name, var->getStrValue()));
					}
					break;
				case symbol_type::WORK:
					{
            mactbl.insert(MacroPair(name, name + ".workdef"));
					}
					break;
				case symbol_type::FUNCTION:
					{
            mactbl.insert (MacroPair(name, name + ".function"));
					}
					break;
			}

			cit++;
		}
	}
}
