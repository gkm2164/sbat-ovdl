/*

Maybe alternative relation

*/

#include <vector>

using namespace std;

#include <sbat.console.hh>
#include <sbat.defines.hh>
#include <sbat.const.exclusive_or.hh>
#include <sbat.constraint.hh>
#include <sbat.binding.hh>
#include <sbat.fragment.hh>

#include <sbat.symboltable.hh>
#include <sbat.symboltable.type.hh>

typedef vector<SBAT::ABind *> abind_vector;
typedef SBAT::symbol_type stype;

namespace SBAT {
  ExclusiveOR::ExclusiveOR(int opcode, vector<Argument *> *flist)
      :Constraint (opcode, flist) {
  }

  bool
  ExclusiveOR::evaluate(Binding *bind) {
    int mask = arg_type::ARG_VPNAME;
    vector<Argument *> *ops = getOperand();
    vector<string> binded;

    Argument *_op1 = (*ops)[0];

    string op1 = _op1->getSValue();
 
    abind_vector *avect = bind->getBindList();

    int varcnt = 0;

    msg_prn (msg_type::INFO, msg_context::START, "Check constraint for exclusive_or to " + op1, true);

    foreach (abind_vector, ait, avect) {
      ABind *elem = *ait;
      string refName = op1 + "." + elem->second;
      
      Variant *v = (Variant *)current_stable->findByName(refName, stype::VARIANT);
      if (v == NULL) continue;
      VariablePoint *vp = v->getParent();

      if (vp->getName() == op1) {
        binded.push_back (refName);
        varcnt++;
      }
    }

    if (varcnt > 1) {
      cout << "SBAT >> Constraint violated due to elements (" << binded[0];
      for (int i = 1; i < binded.size(); i++) {
        cout << ", " << binded[i];
      }
      cout << ")" << endl;

      return false;
    } else {
      return true;
    }

    return !(varcnt > 1);
  }
}
