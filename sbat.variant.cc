#include <string>

using namespace std;

#include <sbat.variant.hh>
#include <sbat.variable_point.hh>

#include <sbat.symboltable.hh>

namespace SBAT {
  Variant::Variant (string name, bool isroot):
    Fragment(Frag_V, name) {
    this->isroot = isroot;
    real_name = name.substr(1);
    def_name = name.substr(1);
    init_vars(name);

  }

  Variant::Variant (string name, VariablePoint *_parent):
    Fragment(Frag_V, name), isroot(false), parent(_parent) {
    
    real_name = parent->getPrefix() + "." + name.substr(1);
    def_name = parent->getPrefix() + "_" + name.substr(1);
    init_vars(parent->getName() + "." + name);
  }
  
  extern string INPUT_EXTENSION_NAME;
  extern string OUTPUT_EXTENSION_NAME;

  void Variant::init_vars(string init_name) {
    var_name = new Variable (init_name + ".name");
    var_name->setValue (real_name);
    var_def_name = new Variable (init_name + ".defname");
    var_def_name->setValue (def_name);
    src = new Variable (init_name + ".src");
    src->setValue (real_name + INPUT_EXTENSION_NAME);
    obj = new Variable (init_name + ".obj");
    obj->setValue (real_name + OUTPUT_EXTENSION_NAME);

    current_stable->removeIfExist(init_name + ".name");
    current_stable->removeIfExist(init_name + ".defname");
    current_stable->removeIfExist(init_name + ".src");
    current_stable->removeIfExist(init_name + ".obj");

    current_stable->insertNew(init_name + ".name", symbol_type::STRING, var_name);
    current_stable->insertNew(init_name + ".defname", symbol_type::STRING, var_def_name);
    current_stable->insertNew(init_name + ".src", symbol_type::STRING, src);
    current_stable->insertNew(init_name + ".obj", symbol_type::STRING, obj);
  }
}
