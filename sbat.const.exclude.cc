#include <iostream>
#include <vector>

using namespace std;

#include <sbat.defines.hh>
#include <argument/sbat.argument.hh>
#include <argument/sbat.arg.vname.hh>
#include <argument/sbat.arg.vpname.hh>
#include <argument/sbat.arg.reference.hh>
#include <sbat.const.exclude.hh>
#include <sbat.fragment.hh>
#include <sbat.constraint.hh>
#include <sbat.binding.hh>

typedef vector<SBAT::ABind *> abind_vector;

namespace SBAT {
  Exclude::Exclude(int opcode, vector<Argument *> *flist)
    :Constraint(opcode, flist) {
  }

  bool
  Exclude::evaluate(Binding *bind) {
    int mask = arg_type::ARG_VNAME | arg_type::ARG_VPNAME | arg_type::ARG_REFERENCE;
    vector<Argument *> *ops = getOperand();
    Argument *_op1 = (*ops)[0];
    Argument *_op2 = (*ops)[1];

    cout << "Check exclude for " << bind->getName() << endl;
    
    if (!(_op1->getType () & mask)) {
      cout << "First operand is not feature name" << endl;
    }
    
    if (!(_op2->getType () & mask)) {
      cout << "Second operand is not feature name" << endl;
    }

    string op1 = "", op2 = "";

    op1 = _op1->getSValue();
    op2 = _op2->getSValue();

    cout << "Check exclude between " << op1 << ", " << op2 << endl;
    
    abind_vector *avect = bind->getBindList();
    abind_vector::const_iterator ait = avect->begin();

    bool op1_exist = false,
         op2_exist = false;

    foreach (abind_vector, ait, avect) {
      ABind *elem = *ait;

      if (op1 == elem->second) {
        op1_exist = true;
      }

      if (op2 == elem->second) {
        op2_exist = true;
      }
    }

    return xorOP(op1_exist, op2_exist) ||
        notOP(orOP(op1_exist, op2_exist));
  }
}
