#ifndef __HEADER_H__
#define __HEADER_H__

double add (double, double);
double sub (double, double);
double div (double, double);
double mult (double, double);
extern double (*ext1) (double, double);
extern double (*ext2) (double, double);

#endif
