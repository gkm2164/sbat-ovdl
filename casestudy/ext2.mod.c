#include "header.h"

double mod (double a, double b) {
  return ((int) a) % ((int)b);
}

double (*ext2)(double a, double b) = mod;
