#include <stdio.h>
#include "header.h"

#define tostr(__x) #__x

int main () {
  double a = 3;
  double b = 4;

  printf("add: %f\n", add(a, b));
  printf("sub: %f\n", sub(a, b));
  printf("mult: %f\n", mult(a, b));
  printf("div: %f\n", div(a, b));
  printf(tostr(ext1)": %f\n", ext1 (a, b));
  printf(tostr(ext2)": %f\n", ext2 (a, b));
  return 0;
}
