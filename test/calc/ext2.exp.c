#include <math.h>
#include "header.h"

double exps(double a, double b) {
  if (b == 0) return 1;
  return a * exps(a, b - 1);
}

double (*ext2)(double a, double b) = exps;
