#include <stdio.h>

#include "header.h"

const char *boolToString(bool);

int main() {
  printf ("Image viewer is consist of following features\n");

  printf ("viewer = true\n");
  printf ("zoom in/out = true\n");
  printf ("cut = %s\n", boolToString(cut()));
  printf ("rotate = %s\n", boolToString(rotate()));
  printf ("resize = %s\n", boolToString(resize()));
  printf ("help = true\n");

  return 0;
}

const char *boolToString(bool data) {
  return (data ? "true" : "false");
}
