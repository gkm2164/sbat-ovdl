#ifndef __HEADER_H__
#define __HEADER_H__

typedef enum { false = 0, true } bool;

#ifndef iv_ed_cut
#define cut() false
#else 
extern bool cut();
#endif

#ifndef iv_ed_rotate
#define rotate() false
#else
extern bool rotate();
#endif

#ifndef iv_ed_resize
#define resize() false
#else
extern bool resize();
#endif

#endif
