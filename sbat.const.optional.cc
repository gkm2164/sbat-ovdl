#include <vector>

using namespace std;

#include "sbat.const.optional.hh"
#include "sbat.fragment.hh"
#include "sbat.binding.hh"
#include "sbat.constraint.hh"

namespace SBAT {
  Optional::Optional(int opcode, vector<Argument *> *flist)
    :Constraint (opcode, flist) {
  }

  bool 
  Optional::evaluate(Binding *binding) {
    return true;
  }
}
