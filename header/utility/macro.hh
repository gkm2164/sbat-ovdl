#ifndef __MACRO_HH__
#define __MACRO_HH__

#include <iostream>
#include <string>
#include <map>
#include <sstream>

using namespace std;

typedef map<string, string> MacroTable;
typedef pair<string, string> MacroPair;

class Macro {
  private:
    map<string, string> *macro_tbl;
    string source;
    stringstream sout;

    int replaceString(int pos);

  public: 
    Macro (MacroTable *macro_table, string _source);
    string replaceString();
    string toConfigFile(string filename);
};

#endif
