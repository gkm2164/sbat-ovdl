#ifndef __VARIABLE_POINT_HH__
#define __VARIABLE_POINT_HH__

#include <vector>
#include <string>

#include "sbat.fragment.hh"
#include "sbat.variant.hh"

using namespace std;

namespace SBAT {
  class VariablePoint: public Fragment {
    private:
      vector<Fragment *> *frags;
      string prefix;

    public:
      VariablePoint(string name):
        Fragment(Frag_VP, name) {

        this->frags = new vector<Fragment *> ();
        prefix = name.substr(2);
      }

      void addVariants(vector<string> *v);
      string getPrefix() { return prefix; }
  };
}

#endif
