#ifndef __SBAT_CONST_MANDATORY_HH__
#define __SBAT_CONST_MANDATORY_HH__

#include <vector>

using namespace std;

#include "sbat.fragment.hh"
#include "sbat.binding.hh"
#include "sbat.constraint.hh"

namespace SBAT {
  class Mandatory: public Constraint {
    public:
      Mandatory(int opcode, vector<Argument *> *flist);
      bool evaluate(Binding *);
  };
}

#endif
