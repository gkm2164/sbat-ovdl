#ifndef __SBAT_CONST_INCLUSIVE_OR_HH__
#define __SBAT_CONST_INCLUSIVE_OR_HH__

#include <vector>

using namespace std;

#include "sbat.fragment.hh"
#include "sbat.binding.hh"
#include "sbat.constraint.hh"

namespace SBAT {
  class InclusiveOR: public Constraint {
    public:
      InclusiveOR(int opcode, vector<Argument *> *flist);
      bool evaluate(Binding *);
  };

}

#endif
