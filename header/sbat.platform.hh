#ifndef __PLATFORM_HH__
#define __PLATFORM_HH__

#include <string>
#include <vector>

using namespace std;

namespace SBAT {
  class Platform { 
    private:
      vector<string> *elems;

    public:
      Platform(vector<string> *elems);
 			vector<string>::const_iterator getIterator(); 
 			vector<string>::const_iterator getEnd();
  };
}
#endif
