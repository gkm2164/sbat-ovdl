#ifndef __SBAT_DEFINES_HH__
#define __SBAT_DEFINES_HH__

#define foreach(__type, __var, __vptr) \
  for (__type::const_iterator (__var) = (__vptr)->begin();\
       (__var) != (__vptr)->end(); \
       (__var)++)

#endif
