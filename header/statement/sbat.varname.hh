#ifndef __SBAT_VARNAME_HH__
#define __SBAT_VARNAME_HH__

#include <sbat.variable.hh>

#include <statement/sbat.symbolexp.hh>

namespace SBAT {
  class VarNameExp: public SymbolExp {
    private:
      int expectType;
      string name;
      Variable *var;

    public:
      VarNameExp(string _name);
      Expression& eval();

      Expression& operator=(Expression &x);

      string getName() { return name; }
      void setExpectType(int type);
      string toString() {
        Expression& ev = eval();
        
        return ev.toString();
      }
  };
}

#endif

