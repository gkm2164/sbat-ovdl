#ifndef __SBAT_REFERENCE_HH__
#define __SBAT_REFERENCE_HH__

#include <string>

using namespace std;

#include <statement/sbat.symbolexp.hh>

namespace SBAT {
  class ReferenceExp: public SymbolExp {
    private:
      string name;
    public:
      ReferenceExp(string _name);
      string getName () { return name; }
      Expression &eval();
  };
}

#endif
