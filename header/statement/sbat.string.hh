#ifndef __SBAT_STRING_HH__
#define __SBAT_STRING_HH__

#include <string>

using namespace std;

#include <statement/sbat.termexp.hh>
#include <statement/sbat.number.hh>

namespace SBAT {
  class StringExp: public TermExp {
    private:
      string value;

    public:
      StringExp (string _value);
      string toString () { return value; }

      Expression& operator! () {
        return *(new NumberExp(value != ""));
      }
      Expression& operator< (Expression &);
      Expression& operator> (Expression &);
      Expression& operator== (Expression &);
      Expression& operator<= (Expression &);
      Expression& operator>= (Expression &);
      Expression& operator!= (Expression &);
      Expression& operator&& (Expression &);
      Expression& operator|| (Expression &);
      Expression& operator+ (Expression &);
      Expression& operator- (Expression &);
      Expression& operator* (Expression &);
      Expression& operator/ (Expression &);
      Expression& eval() { return *this; }
  };
}

#endif
