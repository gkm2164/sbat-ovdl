#ifndef __SBAT_SUBSTEXP_HH__
#define __SBAT_SUBSTEXP_HH__

#include <string>

using namespace std;

#include <statement/sbat.expression.hh>

namespace SBAT {
	class SubstExp: public Expression {
		private:
			Expression *x;
      Expression *y;

		public:
      SubstExp(Expression *, Expression *);
			Expression& eval ();
      void doStmt();

	};
}

#endif
