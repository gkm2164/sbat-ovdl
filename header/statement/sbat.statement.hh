#ifndef __SBAT_STATEMENT_HH__
#define __SBAT_STATEMENT_HH__

#include <vector>

using namespace std;

namespace SBAT {

	class Statement {
		private:
		public:
			Statement();
			virtual void doStmt();
	};
	typedef vector<Statement *> StmtList;
}

#endif
