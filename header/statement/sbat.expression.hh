#ifndef __SBAT_EXPRESSION_HH__
#define __SBAT_EXPRESSION_HH__

#include <string>
#include <vector>

using namespace std;

#include <statement/sbat.statement.hh>

namespace SBAT {
	struct EXP {
		enum {
			SYMBOL = -0x1,
      TERM = -0x2
		};
	};
	
	class TermExp;
	class NumberExp;
	class StringExp;

	class Expression: public Statement {
		private:
			int opcode;
    protected:
      Expression *res;
      int result;
		public:

			Expression(int _opcode);
			struct compare {
				enum comparetype {
					LT, GT, EQ, LE, GE, NE, AND, OR
				};
			};

			int getOPCode();
      virtual Expression& eval();
	    virtual void doStmt();

      virtual string toString() { return ""; }
      virtual int toInteger() { return -9999; }

      virtual Expression& operator= (Expression &);
      virtual Expression& operator< (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator> (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator== (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator<= (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator>= (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator!= (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator&& (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator|| (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator! () { return *(new Expression(0)); }
      virtual Expression& operator+ (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator- (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator* (Expression &) { return *(new Expression(0)); }
      virtual Expression& operator/ (Expression &) { return *(new Expression(0)); }
  };
}

#endif
