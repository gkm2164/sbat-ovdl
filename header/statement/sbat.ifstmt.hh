#ifndef __SBAT_IFSTMT_HH__
#define __SBAT_IFSTMT_HH__

#include <string>
#include <vector>

using namespace std;

#include <statement/sbat.statement.hh>
#include <statement/sbat.expression.hh>

namespace SBAT {
	class IFStatement:public Statement {
		private:
			Expression *compare;
			std::vector<Statement *> *stmts;
			std::vector<IFStatement *> *elseifs;
			std::vector<Statement *> *elsestmts;
	
		public:
			IFStatement(Expression *, std::vector<Statement *> *);
			IFStatement(Expression *, std::vector<Statement *> *, std::vector<Statement *> *); 
			IFStatement(Expression *, std::vector<Statement *> *, std::vector<IFStatement *> *);
			IFStatement(Expression *, std::vector<Statement *> *, std::vector<IFStatement *> *, std::vector<Statement *> *);

      bool test();
      void doStmt();
	};
}

#endif
