#ifndef __SBAT_TERMEXP_HH__
#define __SBAT_TERMEXP_HH__

#include <string>

using namespace std;

#include <statement/sbat.expression.hh>

namespace SBAT {
  struct TERM {
    enum {
      NUMBER = 0x100,
      STRING = 0x101
    };
  };

	class TermExp: public Expression {
		private:
      int termID;

		public:
			TermExp(int opcode);
      int getType() { return termID; }
	};
}

#endif
