#ifndef __SBAT_WHILESTMT_HH__
#define __SBAT_WHILESTMT_HH__

#include <statement/sbat.statement.hh>
#include <statement/sbat.expression.hh>

namespace SBAT {
	class WHILEStatement: public Statement {
		private:
			Expression *cond;
			StmtList *stmts;
		public:
			WHILEStatement (Expression *e, StmtList *s);

	};
}

#endif
