#ifndef __SBAT_FUNCTABLE_HH__
#define __SBAT_FUNCTABLE_HH__

#include <string>
#include <map>

using namespace std;

#include <statement/sbat.expression.hh>

namespace SBAT {
  
  typedef void (*fcall)(std::vector<Expression *> *arglist, void *aux);

  class FunctionTable {
    private:
      map<string, fcall> funcmap;

    public:
      FunctionTable();

      void registFunc(string, fcall);
      fcall getFunction(string);
  };

  extern FunctionTable builtin_func;
}

#endif
