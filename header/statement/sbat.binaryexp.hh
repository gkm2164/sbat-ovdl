#ifndef __SBAT_BINARYEXP_HH__
#define __SBAT_BINARYEXP_HH__

#include <statement/sbat.expression.hh>

namespace SBAT {
	class BinaryExp: public Expression {
		private:
			Expression *x;
			Expression *y;
	
		public:
			BinaryExp(int opcode, Expression *_x, Expression *_y);
      Expression& eval ();
	};
}

#endif
