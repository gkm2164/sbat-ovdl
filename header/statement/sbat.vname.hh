#ifndef __SBAT_VNAME_HH__
#define __SBAT_VNAME_HH__

#include <statement/sbat.symbolexp.hh>

namespace SBAT {
	class VNameExp:public SymbolExp {
		private:
			string name;

		public:
			VNameExp(string name);
      Expression &eval();
	};
}

#endif
