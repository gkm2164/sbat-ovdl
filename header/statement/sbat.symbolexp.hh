#ifndef __SBAT_SYMBOLEXP_HH__
#define __SBAT_SYMBOLEXP_HH__

#include <string>

using namespace std;

#include <statement/sbat.expression.hh>

namespace SBAT {
	struct SYM {
		enum {
			VNAME,
			VPNAME,
			VARIABLE, 
			REFERENCE,
			NUMBER,
			STRING
		};
	};
	class SymbolExp: public Expression {
		private:
			int type;

		public:
			SymbolExp(int _type);
			int getType();
	};
}

#endif
