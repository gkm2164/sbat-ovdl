#ifndef __SBAT_FORSTMT_HH__
#define __SBAT_FORSTMT_HH__

#include <statement/sbat.statement.hh>
#include <statement/sbat.expression.hh>

namespace SBAT {
	class FORStatement: public Statement {
		private:
			Expression *first;
			Expression *cond;
			Expression *after;
			StmtList *stmts;

		public:
			FORStatement (Expression *, Expression *, Expression *, StmtList *);
	};
}


#endif
