#ifndef __SBAT_VPNAME_HH__
#define __SBAT_VPNAME_HH__

#include <statement/sbat.symbolexp.hh>

namespace SBAT {
  class VPNameExp: public SymbolExp {
    private:
      string name;
    public:
      VPNameExp(string name);
  };
}

#endif
