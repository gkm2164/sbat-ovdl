#ifndef __SBAT_FUNCCALLEXP_HH__
#define __SBAT_FUNCCALLEXP_HH__

#include <statement/sbat.expression.hh>
#include <statement/sbat.symbolexp.hh>

namespace SBAT {
	class FuncCallExp: public Expression {
		private:
			SymbolExp *ref;
			std::vector<Expression *> *arglist;

		public:
      FuncCallExp(SymbolExp *ref, std::vector<Expression *> *_arglist);
			Expression& eval();
			void doStmt() { eval(); }
	};
}

#endif
