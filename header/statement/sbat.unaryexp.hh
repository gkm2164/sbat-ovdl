#ifndef __SBAT_UNARYEXP_HH__
#define __SBAT_UNARYEXP_HH__

#include <statement/sbat.expression.hh>

namespace SBAT {
	class UnaryExp: public Expression {
		private:
			Expression *x;

		public:
			UnaryExp(int opcode, Expression *_x);
			Expression& eval();
	};
}

#endif
