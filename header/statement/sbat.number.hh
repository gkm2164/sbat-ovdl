#ifndef __SBAT_NUMBER_HH__
#define __SBAT_NUMBER_HH__

#include <sstream>

using namespace std;

#include <statement/sbat.termexp.hh>

namespace SBAT {
	class NumberExp: public TermExp {
		private:
			int value;

		public:
			NumberExp(int _value);
      string toString();
      int toInteger();
    
      Expression& operator! () { return *(new NumberExp(!value)); }
      Expression& operator< (Expression &);
      Expression& operator> (Expression &);
      Expression& operator== (Expression &);
      Expression& operator<= (Expression &);
      Expression& operator>= (Expression &);
      Expression& operator!= (Expression &);
      Expression& operator&& (Expression &);
      Expression& operator|| (Expression &);
      Expression& operator+ (Expression &);
      Expression& operator- (Expression &);
      Expression& operator* (Expression &);
      Expression& operator/ (Expression &);

      Expression& eval();
	};
}

#endif
