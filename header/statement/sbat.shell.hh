#ifndef __SBAT_SHELL_HH__
#define __SBAT_SHELL_HH__

#include <string>

using namespace std;

#include <statement/sbat.statement.hh>

namespace SBAT {

  class SHELLStatement:public Statement {
    private:
      string shellName;
      string cmds;

    public:
      SHELLStatement(string, string);
      void doStmt();
  };
}

#endif
