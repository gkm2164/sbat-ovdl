#ifndef __FRAGMENT_HH__
#define __FRAGMENT_HH__

#include <string>

using namespace std;

namespace SBAT {
  enum fragmentType {
    Invalid = -1,
    Frag_V,
    Frag_VP
  };

  class Fragment {
    private:
      int type;
      string name;

    public:
      Fragment(int _type, string _name):
        type(_type), name(_name) {
      }

      int getType() {
        return type;
      }

      string getName () {
        return name;
      }
  };
  class Variant;
  class VariablePoint;
}

#endif
