#ifndef __VARIANTS_HH__
#define __VARIANTS_HH__

#include <string>

using namespace std;

#include <sbat.fragment.hh>
#include <sbat.variable.hh>

namespace SBAT {
  class Variant:public Fragment {
    private:
      bool isroot;
      string real_name;
      string def_name;
      VariablePoint *parent;
      Variable *var_name;
      Variable *var_def_name;
      Variable *src;
      Variable *obj;

    public:
      Variant(string name, bool isroot);
      Variant(string name, VariablePoint *parent);
      void init_vars(string init_name);
      void setSourceName(string name) { real_name = name; }
      string getSourceName() { return real_name; }
      VariablePoint *getParent() { return parent; }
  };
}

#endif
