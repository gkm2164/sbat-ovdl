#ifndef __SBAT_ARG_VARS_HH__
#define __SBAT_ARG_VARS_HH__

#include <string>

using namespace std;

#include <argument/sbat.argument.hh>
#include <sbat.variable.hh>

namespace SBAT {
  class ArgVariableName: public Argument {
    private:
      string name;
      Variable *var;

    public:
      ArgVariableName(string name);
      string getName();
  };
}

#endif
