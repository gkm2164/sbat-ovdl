#ifndef __SBAT_ARG_REFERENCE_HH__
#define __SBAT_ARG_REFERENCE_HH__

#include <string>

using namespace std;

#include <argument/sbat.argument.hh>

namespace SBAT {
  class ArgReference: public Argument {
    private:
      string name;
      

    public:
      ArgReference(string name);
      string getName();
  };
}

#endif
