#ifndef __SBAT_ARG_VNAME_HH__
#define __SBAT_ARG_VNAME_HH__

#include <string>

using namespace std;

#include <sbat.variant.hh>
#include <argument/sbat.argument.hh>

namespace SBAT {
  class ArgVName: public Argument {
    private:
      string name;

    public:
      ArgVName(string name);
      string getName();
  };
}

#endif
