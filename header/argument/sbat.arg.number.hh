#ifndef __SBAT_ARG_NUMBER_HH__
#define __SBAT_ARG_NUMBER_HH__

#include <argument/sbat.argument.hh>

namespace SBAT {
  class ArgNumber: public Argument {
    private:
      int value;

    public:
      ArgNumber(int value);
      int getValue();
  };
}

#endif
