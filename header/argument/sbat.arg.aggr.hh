#ifndef __SBAT_AGGR_HH__
#define __SBAT_AGGR_HH__

#include <vector>

using namespace std;

#include <argument/sbat.argument.hh>

namespace SBAT {
  class ArgVNameList: public Argument {
    private:
      vector<string> *vlist;

    public:
      ArgVNameList(vector<string> *);
  };
}

#endif
