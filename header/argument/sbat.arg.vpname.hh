#ifndef __SBAT_ARG_VPNAME_HH__
#define __SBAT_ARG_VPNAME_HH__

#include <string>

using namespace std;

#include <argument/sbat.argument.hh>

namespace SBAT {
  class ArgVPName: public Argument {
    private:
      string name;

    public:
      ArgVPName(string name);
      string getName();
  };
}

#endif
