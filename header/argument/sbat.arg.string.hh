#ifndef __SBAT_ARG_STRING_HH__
#define __SBAT_ARG_STRING_HH__

#include <string>

using namespace std;

#include <argument/sbat.argument.hh>

namespace SBAT {
  class ArgString: public Argument {
    private:
      string value;

    public:
      ArgString(string value);
      string getValue();
  };
}

#endif
