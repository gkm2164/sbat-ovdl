#ifndef __SBAT_ARGUMENT_HH__
#define __SBAT_ARGUMENT_HH__

#include <string>

using namespace std;

namespace SBAT {
  struct argument_type {
    enum ArgumentType {
      ARG_VNAME = 0x1,
      ARG_VPNAME = 0x2,
      ARG_REFERENCE = 0x4,
      ARG_VARIABLE = 0x8,
      ARG_NUMBER = 0x10,
      ARG_STRING = 0x11,
      ARG_AGGR = 0x12
    };
  };

  class Argument {
    private:
      int type;
			int ivalue;
			string svalue;

    public:
    	Argument(int _type);
      Argument(int _type, int _ivalue);
      Argument(int _type, string _svalue);
      int getType();
      int getIValue() { return ivalue; }
      string getSValue() { return svalue; }
  };
}

typedef SBAT::argument_type arg_type;

#endif
