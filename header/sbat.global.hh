#ifndef __SBAT_GLOBAL_HH__
#define __SBAT_GLOBAL_HH__

#include <sbat.script.hh>
#include <string>
#include <map>


using namespace std;

namespace SBAT {
  class Global {
    private:
      map<string, string> globalvars;

    
    public:
      Global();
      void setGlobalSymbol (string, string);
      void updateSymbolTable();
  };

  extern Script *currentScript;
}

#endif
