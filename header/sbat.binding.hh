#ifndef __SBAT_BINDING_HH__
#define __SBAT_BINDING_HH__

#include <string>
#include <vector>
#include <map>

using namespace std;

#include <sbat.platform.hh>

#include <sbat.variant.hh>
#include <sbat.variable_point.hh>
#include <statement/sbat.expression.hh>


namespace SBAT {
  
  typedef pair<string, string> ABind;
  
  struct ABindType {
    enum {
      SINGLE,
      AGGREGATION
    };
  };

  class ABinding {
    private:
      int type;
      string vpname;
      string vname;
      vector<string> *vnames;


    public:
      ABinding(string _vpname, vector<string> *_vnames);
      ABinding(string _vpname, string _vname);
      
      int getType();

      ABind *getSingleBind();

      vector<ABind *> *getAggrBind();

      static void addBinding(vector<ABind *> *vect, ABinding *bind);
  };

  class Binding {

    private:
      string productName;
      vector<ABind *> *bindlist;
			vector<string> *binds;
			map<string, string> *codes;
      string def_str;

      Variable *var_name;
      Variable *var_def_str;
      Variable *bindings;
      Variable *srcs;
      Variable *objs;

    public:
      Binding(string productName, vector<ABind *> *bindlist);
		
			void expandBinding(Platform *);
      vector<string> *getBinds();
      vector<ABind *> *getBindList();
      string getName() { return productName; }
			void setBindingCodes();
      void init_vars(string init_name);
      string expandSources (string refs);
      void builtin_compile(std::vector<Expression *> *arglist);
      static void _builtin_compile(std::vector<Expression *> *arglist, void *ptr);
  };

}

#endif
