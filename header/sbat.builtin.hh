#ifndef __SBAT_BUILTIN_HH__
#define __SBAT_BUILTIN_HH__

#include <vector>

#include <statement/sbat.expression.hh>
#include <statement/sbat.functable.hh>


namespace SBAT {
  typedef void ftype(std::vector<Expression *> *arglist, void *aux);
  
  ftype builtin_print;
  ftype builtin_compile;
  ftype builtin_link;
}

#endif
