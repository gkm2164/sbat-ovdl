#ifndef __SBAT_CONST_EXCLUDE_HH__
#define __SBAT_CONST_EXCLUDE_HH__

#include <vector>

using namespace std;

#include <argument/sbat.argument.hh>
#include "sbat.fragment.hh"
#include "sbat.binding.hh"
#include "sbat.constraint.hh"

namespace SBAT {
  class Exclude: public Constraint {
    public:
      Exclude(int opcode, vector<Argument *> *flist);
      bool evaluate(Binding *);
  };
}

#endif
