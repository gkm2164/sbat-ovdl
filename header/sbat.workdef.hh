#ifndef __SBAT_WORKDEF_HH__
#define __SBAT_WORKDEF_HH__

#include <string>
#include <vector>

using namespace std;

#include <sbat.binding.hh>
#include <statement/sbat.statement.hh>
#include <sbat.symboltable.hh>

namespace SBAT {
  class WorkDef;

	class WorkDef {
		private:
			string name;
      vector<string> *on;
			StmtList *stmts;
			vector<string> *dep;
			SymbolTable *work_st;
      Variant *current_elem;
      Binding *binding;
      bool alreadyRun;

		public:
			WorkDef(string name, vector<string> *on, StmtList *stmts);

			void add_dependency(vector<string> *str);
      vector<string> *getDeps();
      string getName();
      
      void init();
      void runStmt();
      bool isAlreadyExecuted() { return alreadyRun; }
      void run(Variant *current_elem, Binding *bind);

      void setLocalSymbolTable(SymbolTable *st) { work_st = st; }
      SymbolTable *getLocalSymbolTable() { return work_st; }
	};
}

#endif
