#ifndef __SCRIPT_HH__
#define __SCRIPT_HH__

#include <map>
#include <vector>

using namespace std;

#include <sbat.platform.hh>
#include <sbat.variable_point.hh>
#include <sbat.constraint.hh>
#include <sbat.binding.hh>
#include <sbat.workdef.hh>

#include "sbat.symboltable.hh"

namespace SBAT {
  class Script {
    private:
      Platform *pf;
      vector<VariablePoint *> *vpdl;
      vector<Constraint *> *constl;
      vector<Binding *> *bdecl;
      vector<WorkDef *> *wdefs;

    public:
      Script(Platform *pf, vector<VariablePoint *> *,
      	vector<Constraint *> *, vector<Binding *> *, vector<WorkDef *> *);
			bool applyBinding();
      bool runScript();
  };
}
#endif
