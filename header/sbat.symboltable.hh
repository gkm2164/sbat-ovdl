#ifndef __SBAT_SYMBOLTABLE_HH__
#define __SBAT_SYMBOLTABLE_HH__

#include <map>
#include <string>
#include <stack>

using namespace std;

#include <sbat.classdef.hh>

#include <sbat.fragment.hh>
#include <sbat.variant.hh>
#include <sbat.variable_point.hh>
#include <sbat.workdef.hh>

#include <sbat.symboltable.type.hh>

#include <utility/macro.hh>

namespace SBAT {
	
	class Symbol {
		private:
			int type;
			void *ptr;
      WorkDef *trigger;

		public:
			Symbol(int _type, void *_ptr): type(_type), ptr(_ptr) { trigger = NULL; }
			int getType() { return type; }
			void *getData() { return ptr; }
      void setTrigger(WorkDef *wd) { trigger = wd; }
      WorkDef *getTrigger() { return trigger; }
	};

  class SymbolTable {
    private:
    	SymbolTable *parent;
      map<string, Symbol *> *frags;
			
    public:
      SymbolTable();
      SymbolTable(SymbolTable *parent);
		
			void insertNew(string name, int type, void *ptr);
      void removeIfExist(string name);
			void *findByName(string name, int type);

      bool setTrigger(string name, WorkDef *ptr);
      WorkDef *getTrigger(string name);
 			SymbolTable *getParent();

 			void toMacroTable(MacroTable &mactbl);

      int getSymbolType(string name);

      void showElements();
  };

  void createNewSymbolTable();
  SymbolTable *recoverOldSymbolTable();

  extern SymbolTable *current_stable;
  extern stack<SymbolTable *> stable_stack;
}

#endif
