#ifndef __SBAT_SCANNER_H__
#define __SBAT_SCANNER_H__

#include "sbat.tab.hh"

namespace SBAT {
  class Scanner {
    private:

    public:
      Scanner () { }
      int lex(Parser::semantic_type *yylval,
              Parser::location_type *yylloc);
  };
}

#endif
