#ifndef __SBAT_SYMBOLTABLE_TYPE_HH__
#define __SBAT_SYMBOLTABLE_TYPE_HH__

namespace SBAT {
	struct symbol_type {
		enum {
			VARIANT,
			VARIABLE_POINT,
      VARIABLE,
			BINDING,
			NUMBER,
			STRING,
			WORK,
			FUNCTION
		};
	};
}

#endif
