#ifndef __SBAT_CONSTRAINT_HH__
#define __SBAT_CONSTRAINT_HH__

#include <string>
#include <vector>

#include <argument/sbat.argument.hh>
#include <sbat.binding.hh>

using namespace std;

enum {
  UNKNOWN = -1,
  MANDATORY,
  OPTIONAL,
  REQUIRE,
  EXCLUDE,
  INCLUSIVE_OR,
  EXCLUSIVE_OR
};

namespace SBAT {
  class Constraint {
    private:
      int opcode;
      vector<Argument *> *operands;

    public:
      Constraint(int opcode, vector<Argument *> *ops);
      virtual bool evaluate(Binding *);
      static Constraint *newConstraint(int opcode, vector<Argument *> *arglist);
      vector<Argument *> *getOperand() { return operands; }

      bool andOP(bool a, bool b) { return a && b; }
      bool orOP(bool a, bool b) { return a || b; }
      bool xorOP(bool a, bool b) { return (!a && b) || (a && !b); }
      bool notOP(bool a) { return !a; }
  };
}

#endif
