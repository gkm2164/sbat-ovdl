#ifndef __SBAT_VARIABLE_HH__
#define __SBAT_VARIABLE_HH__

#include <string>

using namespace std;

namespace SBAT {
  struct VARTYPE {
    enum {
      INTEGER = 0x1,
      STRING = 0x2
    };
  };
  class Variable {
    private:
      int vartype;
      string name;
      int ivalue;
      string svalue;

    public:
      Variable(string _name):name(_name) {
        vartype = VARTYPE::INTEGER;
        ivalue = 0;
      }

      string getName() { return name; }

      void setType(int type) {
        vartype = type;
      }

      int getType() {
        return vartype;
      }

      void setValue(int ival) {
        vartype = VARTYPE::INTEGER;
        ivalue = ival;
      }
      void setValue(string sval) {
        vartype = VARTYPE::STRING;
        svalue = sval;
      }

      int getIntValue() {
        return ivalue;
      }

      string getStrValue() {
        return svalue;
      }
  };
}

#endif
