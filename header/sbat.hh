#ifndef __SBAT_HH__
#define __SBAT_HH__

#include <iostream>
#include <algorithm>
#include <string>
#include <vector>

#include <sbat.console.hh>
#include <sbat.script.hh>

#include <sbat.fragment.hh>
#include <sbat.variant.hh>
#include <sbat.variable_point.hh>

#include <sbat.codemap.hh>
#include <sbat.platform.hh>
#include <sbat.constraint.hh>

#include <sbat.binding.hh>

#include <argument/sbat.argument.hh>
#include <argument/sbat.arg.number.hh>
#include <argument/sbat.arg.reference.hh>
#include <argument/sbat.arg.string.hh>
#include <argument/sbat.arg.vname.hh>
#include <argument/sbat.arg.vpname.hh>
#include <argument/sbat.arg.vars.hh>
#include <argument/sbat.arg.aggr.hh>

#include <sbat.workdef.hh>

#include <sbat.variable.hh>

#include <statement/sbat.statement.hh>
#include <statement/sbat.ifstmt.hh>
#include <statement/sbat.forstmt.hh>
#include <statement/sbat.whilestmt.hh>
#include <statement/sbat.expression.hh>
#include <statement/sbat.termexp.hh>

#include <statement/sbat.unaryexp.hh>
#include <statement/sbat.binaryexp.hh>
#include <statement/sbat.funccallexp.hh>
#include <statement/sbat.substexp.hh>

#include <statement/sbat.vname.hh>
#include <statement/sbat.vpname.hh>
#include <statement/sbat.varname.hh>
#include <statement/sbat.reference.hh>
#include <statement/sbat.number.hh>
#include <statement/sbat.string.hh>
#include <statement/sbat.shell.hh>

#include <sbat.symboltable.hh>
#include <sbat.symboltable.type.hh>


using namespace std;

#endif
