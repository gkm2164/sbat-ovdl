#ifndef __SBAT_CONSOLE_HH__
#define __SBAT_CONSOLE_HH__

#include <string>

using namespace std;

namespace SBAT {
  struct msg_type {
    enum {
      DEBUG   = 0x0,
      INFO    = 0x1,
      WARNING = 0x2,
      ERROR   = 0x3,
    };
  };

  struct msg_context {
    enum {
      START = 0x0,
      MIDDLE = 0x1,
    };
  };
  void msg_prn (int level, int msg_context, string msg, bool linefeed);
}

#endif

