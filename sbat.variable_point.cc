#include "sbat.symboltable.hh"
#include "sbat.variable_point.hh"

namespace SBAT {
  extern vector<string> all_variants;

  void
  VariablePoint::addVariants(vector<string> *v) {
    vector<string>::const_iterator it = v->begin();

    while (it != v->end()) {
      string name = *it;
      string refName = getName() + ".";
      refName += name;

      Variant *frag = new Variant(name, this);
      frag->init_vars (refName);
      
      frags->push_back (frag);
      all_variants.push_back(refName);

      current_stable->insertNew(refName, symbol_type::VARIANT, frag);
      it++;
    }
  }
}
