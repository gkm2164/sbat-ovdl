#include <iostream>

using namespace std;

#include <sbat.console.hh>

int debug_level = SBAT::msg_type::WARNING;
namespace SBAT {
  string msgtype[4] = { "debug", "info", "warning", "error" };

  void msg_prn (int level, int msg_context, string msg, bool linefeed) {
    string msg_prefix = "sbat[" + msgtype[level] + "]: ";
    if (level < debug_level) { return; }
    if (msg_context == msg_context::START) {
      cout << msg_prefix;
    }
    cout << msg;
    if (linefeed == true) {
      cout << endl;
    }
  }
}
