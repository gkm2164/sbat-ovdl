#include <assert.h>

#include <iostream>
#include <string>
#include <vector>
#include <typeinfo>

using namespace std;

#include <sbat.console.hh>
#include <sbat.workdef.hh>
#include <sbat.variable.hh>
#include <sbat.symboltable.hh>
#include <sbat.symboltable.type.hh>
#include <statement/sbat.statement.hh>

namespace SBAT {
	typedef symbol_type stype;
  extern string current_elem;
  extern string current_binding;

	WorkDef::WorkDef(string _name, vector<string> *_on, StmtList *_stmts):
		name(_name), on(_on), stmts(_stmts) {
	  alreadyRun = false;
    dep = new vector<string> ();
    work_st = new SymbolTable(current_stable);
    if ((_on->size() == 1) && ((*_on)[0] == "all_variants")) {
      // If the workdef is defined on every variants
      extern vector<string> all_variants;
      for (int i = 0; i < all_variants.size(); i++) {
        current_stable->setTrigger(all_variants[i], this);  
      }
    } else {
      for (int i = 0; i < on->size(); i++) {
        current_stable->setTrigger ((*on)[i], this);
      }
    }
  }

	void
	WorkDef::add_dependency (vector<string> *strs) {
		dep = strs;
	}
 
  string
  WorkDef::getName() { return name; }

  vector<string> *
  WorkDef::getDeps() { return dep; }

  void
  WorkDef::init() {
    alreadyRun = false;
  }

  void
  WorkDef::runStmt() {
    alreadyRun = true;
    if (current_elem != NULL) {
      Variant *var = (Variant *)current_stable->findByName(current_elem->getName(), symbol_type::VARIANT);
      if (var == NULL) {
        msg_prn (msg_type::DEBUG, msg_context::START, "Current element" + current_elem->getName(), true);
      }
      
      msg_prn (msg_type::DEBUG, msg_context::START, "WorkDef runs on " + current_elem->getName(), true);

      current_stable->removeIfExist("current_variant");
      current_stable->insertNew ("current_variant", symbol_type::VARIANT, current_elem);
      current_elem->init_vars("current_variant");
    }

    current_stable->removeIfExist("current_binding");
    current_stable->insertNew ("current_binding", symbol_type::BINDING, binding);
    binding->init_vars("current_binding");

    /* Execution */
    for (int i = 0; i < stmts->size(); i++) {
      Statement *stmt = (*stmts)[i];
      stmt->doStmt();
    }
  }

  void
  WorkDef::run(Variant *_current_elem, Binding *_binding) {
    stable_stack.push(current_stable);
    current_stable = work_st;
    vector<string>::const_iterator it = dep->begin();

    current_elem = _current_elem;
    binding = _binding;
    for (; it != dep->end(); it++) {
      string name = *it;
      WorkDef *elem = (WorkDef *)current_stable->findByName(name, symbol_type::WORK);
      
      if (!elem->isAlreadyExecuted()) {
        elem->run(current_elem, binding);
      }
    }

    runStmt();
    current_stable = stable_stack.top();
    stable_stack.pop();
  }
}
