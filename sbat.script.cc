#include <unistd.h>

#include <iostream>
#include <vector>

using namespace std;

#include <sbat.console.hh>
#include <sbat.script.hh>

#include <sbat.platform.hh>
#include <sbat.variable_point.hh>
#include <sbat.constraint.hh>
#include <sbat.binding.hh>
#include <sbat.workdef.hh>

typedef vector<SBAT::Constraint *> const_vector;
typedef vector<SBAT::Binding *> binding_vector;

extern bool clean;

namespace SBAT {
  Script::Script(Platform *_pf, vector<VariablePoint *> *_vpd,
        vector<Constraint *> *_cdecl, vector<Binding *> *_bdecl, vector<WorkDef *> *_wdefs):
        pf(_pf), vpdl(_vpd), constl(_cdecl), bdecl(_bdecl), wdefs(_wdefs) {
  }

	bool
	Script::applyBinding() {

		for (binding_vector::const_iterator it = bdecl->begin();
				 it != bdecl->end();
				 it++) {

			Binding *elem = *it;
			elem->expandBinding(pf);

      if (constl != NULL) {
        for (const_vector::const_iterator cit = constl->begin();
             cit != constl->end();
             cit++) {
          Constraint *cst = *cit;
          if (cst->evaluate (elem) == false) {
            msg_prn (msg_type::ERROR, msg_context::START, "Constraint check failed!", true);
            return false;
          }
        }
      }
		}

    return true;
	}
  
  bool
  Script::runScript() {
    extern vector<string> all_variants;

    if (clean) {
      for (int i = 0; i < all_variants.size(); i++) {
        Variable *v = (Variable *)current_stable->findByName(all_variants[i] + ".obj", symbol_type::STRING);

        string filename = v->getStrValue ();
        msg_prn (msg_type::INFO, msg_context::START, "Remove " + filename, true);
        unlink (filename.c_str());
      }

      for (binding_vector::const_iterator it = bdecl->begin();
           it != bdecl->end();
           it++) {
        Binding *bind = *it;
        msg_prn (msg_type::INFO, msg_context::START, "Remove " + bind->getName(), true);
        unlink (bind->getName().c_str ());
      }
      return true;
    }

    for (binding_vector::const_iterator it = bdecl->begin();
         it != bdecl->end();
         it++) {
      Binding *bind = *it;
      msg_prn (msg_type::INFO, msg_context::START, "Runscript for [" + bind->getName() + "]", true);
      
      WorkDef *base_wd = (WorkDef *)current_stable->findByName("start", symbol_type::WORK);
      if (base_wd == NULL) {
        msg_prn (msg_type::WARNING, msg_context::START, "start workdef is not defined", true);
      } else {
        base_wd->run(NULL, bind);
      }

      vector<string> *binds = bind->getBinds();
      for (int i = 0; i < binds->size(); i++) {
        string name = (*binds)[i];
        Variant *v = (Variant *)current_stable->findByName(name, symbol_type::VARIANT);
        if (v == NULL) {
          msg_prn (msg_type::INFO, msg_context::START, name + " is not a variant", true);
        }
        WorkDef *wd = current_stable->getTrigger(name);
        
        if (wd != NULL) {
          msg_prn (msg_type::INFO, msg_context::START, "[Trigger run on fragment " + name + "]", true);
          wd->run(v, bind);
        }
      }

      WorkDef *wd = current_stable->getTrigger(bind->getName());
      if (wd != NULL) {
        msg_prn (msg_type::INFO, msg_context::START, "[Trigger run on binding " + bind->getName() + "]", true);
        wd->run(NULL, bind);
      }
    
      WorkDef *final_wd = (WorkDef *)current_stable->findByName("final", symbol_type::WORK);
      if (final_wd == NULL) {
        msg_prn (msg_type::WARNING, msg_context::START, "final workdef is not defined", true);
      } else {
        final_wd->run(NULL, bind);
      }
    
      msg_prn (msg_type::INFO, msg_context::START, "Script run complete for [" + bind->getName() + "]", true);
    }

    return true;   
  }
}
