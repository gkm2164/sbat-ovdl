#include <iostream>
#include <string>

#include <cstdio>
#include <cstdlib>

#include <sbat.hh>
#include <statement/sbat.functable.hh>
#include <sbat.global.hh>
#include "sbat.tab.hh"

#include <sbat.builtin.hh>

using namespace SBAT;

bool file_opened = false;
bool clean = false;
bool gen_template = false;

string filename;
extern FILE *yyin;
extern string EXTENSION_NAME;
extern int debug_level;

Global global;

void help () {
  /* Gyeongmin Go: Edit occured in this place!    */
  cout << "Usage: /path/to/sbat filename [options...]" << endl;
  cout << endl;
  cout << "\t-h or --help: Help message" << endl;
  cout << "\t-d or --debug x, (x: 0 = ALL, 1 = INFO, 2 = WARNING, 3 = ERROR): show debug message where level is greater equal than x" << endl;
  cout << "\t-c or --clean: Clean all objects and executable files where described in scripts" << endl;
  cout << "\t-t or --template: Generate basic template" << endl;
  cout << endl;
}

void parse_option (int argc, char *argv[]) {
  for (int i = 0; i < argc; i++) {
    string opt(argv[i]);
    if (opt[0] == '-') {
      if (opt == "-h" || opt == "--help") {
        help ();
        exit (0);
      } else if (opt == "-d" || opt == "--debug") {
        i++;
        debug_level = atoi (argv[i]);
      } else if (opt =="-c" || opt == "--clean") {
        clean = true; /* Remove at sbat.script.cc >> Script.runScript ()*/
      } else if (opt == "-t" || opt =="--template") {
        FILE *fp = fopen(argv[++i], "w");
        filename = string(argv[i]);
        msg_prn (msg_type::INFO, msg_context::START, "Template generate to " + filename, true);
        fprintf (fp, "Platform: { }\n\n"
                     "VPDec: { }\n\n"
                     "Constraints: { }\n\n"
                     "Binding: { }\n\n"
                     "WorkDef start() {\n}\n\n"
                     "WorkDef final() {\n}\n\n");
        fclose (fp);
        exit(0);
      } else {
        cout << "Unknown options" << endl;
      }
    } else {
      FILE *fp = fopen(argv[i], "r");
      
      if (fp == NULL) {
        filename = string(argv[i]);
        msg_prn (msg_type::ERROR, msg_context::START, "Bad filename!", true);
        exit (-1);
      }
      
      file_opened = true;
      yyin = fp;
    }
  }
}

int main(int argc, char *argv[]) {
  parse_option(argc - 1, argv + 1);
  
  if (!file_opened) {
    msg_prn (msg_type::ERROR, msg_context::START, "Usage: " + string(argv[0]) + " filename [options...]", true);
    return -1;
  }

  current_stable = new SymbolTable();
  builtin_func.registFunc("print", builtin_print);
  builtin_func.registFunc("compile", builtin_compile);
  builtin_func.registFunc("link", builtin_link);

  global.updateSymbolTable();
  Parser parser;

  return parser.parse();
}

