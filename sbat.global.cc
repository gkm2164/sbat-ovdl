#include <string>
#include <map>

using namespace std;

#include <sbat.global.hh>
#include <sbat.symboltable.hh>

namespace SBAT {
  Script *currentScript = NULL;

  string EXTENSION_NAME = ".c";
  string INPUT_EXTENSION_NAME = ".c";
  string OUTPUT_EXTENSION_NAME = ".o";
  vector<string> all_variants;

  Global::Global() {
    globalvars["config_template"] = "config.h.in";
    globalvars["config_output"] = "config.h";
    globalvars["compiler"] = "gcc";
    globalvars["options"] = "-DDEBUG";
  }

  void
  Global::setGlobalSymbol (string name, string value) {
    globalvars.insert(pair<string, string>(name, value));
  }

  void
  Global::updateSymbolTable() {
    map<string, string>::const_iterator cit = globalvars.begin();

    while (cit != globalvars.end()) {
      string key = "global." + cit->first;
      string value = cit->second;
      Variable *var = new Variable(key);
      var->setValue(value);

      current_stable->removeIfExist(key);
      current_stable->insertNew(key, symbol_type::STRING, var);
      cit++;
    }
  }

  
}
