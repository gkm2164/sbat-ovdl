OBJS  = sbat.platform.o
OBJS += sbat.symboltable.o
OBJS += sbat.variant.o
OBJS += sbat.variable_point.o
OBJS += sbat.script.o
OBJS += sbat.constraint.o
OBJS += sbat.binding.o
OBJS += sbat.binding.abinding.o
OBJS += sbat.workdef.o
OBJS += sbat.global.o
OBJS += sbat.create.bindingdoc.o
OBJS += sbat.builtin.o
OBJS += sbat.builtin.compile.o

OBJS += sbat.const.exclude.o
OBJS += sbat.const.exclusive_or.o
OBJS += sbat.const.inclusive_or.o
OBJS += sbat.const.mandatory.o
OBJS += sbat.const.optional.o
OBJS += sbat.const.require.o

OBJS += argument/sbat.argument.o
OBJS += argument/sbat.arg.number.o
OBJS += argument/sbat.arg.reference.o
OBJS += argument/sbat.arg.string.o
OBJS += argument/sbat.arg.vname.o
OBJS += argument/sbat.arg.vpname.o
OBJS += argument/sbat.arg.vars.o
OBJS += argument/sbat.arg.aggr.o

OBJS += statement/sbat.statement.o
OBJS += statement/sbat.ifstmt.o
OBJS += statement/sbat.forstmt.o
OBJS += statement/sbat.whilestmt.o
OBJS += statement/sbat.expression.o

OBJS += statement/sbat.binaryexp.o
OBJS += statement/sbat.unaryexp.o
OBJS += statement/sbat.substexp.o
OBJS += statement/sbat.funccallexp.o
OBJS += statement/sbat.termexp.o
OBJS += statement/sbat.symbolexp.o
OBJS += statement/sbat.shell.o

OBJS += statement/sbat.vname.o
OBJS += statement/sbat.vpname.o
OBJS += statement/sbat.varname.o
OBJS += statement/sbat.reference.o
OBJS += statement/sbat.number.o
OBJS += statement/sbat.string.o
OBJS += statement/sbat.functable.o

OBJS += sbat.console.o
OBJS += main.o
OBJS += sbat.lex.o
OBJS += sbat.tab.o

OBJS += utility/macro.o

SRCS = $(OBJS:.o=.cc)

HEADERS = $(shell find ./header/ -name '*.hh' -print)

FFLAGS = -f -t
BFLAGS = -d $(BISON_DEBUG)

OBJDIR = obj
INCLUDE_DIR = header/

CC = g++
CPPFLAGS += -I $(INCLUDE_DIR) -g
FLEX = flex
BISON = bison

TARGET = sbat

BISON_DEBUG = --report=state --report-file=report.txt

FSRCS = sbat.l
BSRCS = sbat.yy

FLEX_OUT = sbat.lex.cc
BISON_OUT = sbat.tab.cc sbat.tab.hh stack.hh location.hh position.hh

SYSTEM_NAME = $(shell uname -a)
USER_NAME = $(shell whoami)

SBAT_OPTIONS = -d 0

all: $(OBJDIR) $(BISON_OUT) $(TARGET)

$(TARGET): $(addprefix $(OBJDIR)/, $(OBJS))
	$(CC) -g -o $@ $^

$(OBJDIR):
	mkdir $@ $@/statement $@/argument $@/utility

$(OBJDIR)/%.o: %.cc $(HEADERS)
	$(CC) $(CPPFLAGS) -c -o $@ $<

main.cc: $(INCLUDE_DIR)sbat.hh sbat.tab.hh

$(FLEX_OUT): $(FSRCS) $(BISON_OUT)
	$(FLEX) $(FFLAGS) $< > $@

$(BISON_OUT): $(BSRCS) 
	$(BISON) $(BFLAGS) $<

patch:
	patch -p0 < bison_script.patch

.PHONY: clean
clean:
	@echo Removing all object files and executable file.
	@rm -rf $(TARGET) $(addprefix $(OBJDIR)/, $(OBJS)) $(FLEX_OUT) $(BISON_OUT) location.hh position.hh sbat.tab.hh stack.hh report.txt
	@rm -rf obj
	@(cd test/calc && ./$(TARGET) -c calculator.sbat) || echo Force execution of sbat
	@(cd test/image_viewer && ./$(TARGET) -c image_viewer.sbat) || echo Force execution of sbat
	@(cd test_nullvp && ./$(TARGET) -c test.sbat) || echo Force execution of sbat
	@rm -rf test_nullvp/$(TARGET)
	@rm -rf test/calc/$(TARGET)
	@rm	-rf test/image_viewer/$(TARGET)


git_push: git_commit

git_commit: clean
	git add *
	git commit .

test: FORCE
	@echo Test for non-VP contains product line
	make $(TARGET)
	cp ./$(TARGET) test_nullvp/$(TARGET)
	(cd test_nullvp && ./$(TARGET) $(SBAT_OPTIONS) test.sbat)
	@echo Test for VP contains product line
	make $(TARGET)
	@echo   test for Calculator example
	cp ./$(TARGET) test/calc/$(TARGET)
	(cd test/calc && ./$(TARGET) $(SBAT_OPTIONS) calculator.sbat)
	@echo   test for Image_viewer example
	cp ./$(TARGET) test/image_viewer/$(TARGET)
	(cd test/image_viewer && ./$(TARGET) $(SBAT_OPTIONS) image_viewer.sbat)

FORCE:

install: all
	@echo export PATH=\$$PATH:`pwd` >> ~/.bash_profile
