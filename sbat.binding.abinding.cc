#include <string>
#include <vector>

using namespace std;

#include <sbat.defines.hh>
#include <sbat.binding.hh>

namespace SBAT {
  
  ABinding::ABinding(string _vpname, vector<string> *_vnames):
    vpname(_vpname), vnames(_vnames), type(ABindType::AGGREGATION) {}
    
  ABinding::ABinding(string _vpname, string _vname):
    vpname(_vpname), vname(_vname), type(ABindType::SINGLE) {}
  
  int ABinding::getType() { return type; }

  ABind *ABinding::getSingleBind() { return new ABind(vpname, vname); }

  vector<ABind *> *ABinding::getAggrBind() {
    vector<ABind *> *ptr = new vector<ABind *>();
    foreach(vector<string>, ait, vnames) {
      string elem = *ait;
      ptr->push_back(new ABind(vpname, elem));
    }
    
    return ptr;
  }

  void ABinding::addBinding(vector<ABind *> *vect, ABinding *bind) {
    switch (bind->getType()) {
      case ABindType::SINGLE:
        vect->push_back(bind->getSingleBind());
        break;
      case ABindType::AGGREGATION:
        vector<ABind *> *aggr = bind->getAggrBind();
        foreach(vector<ABind *>, ait, aggr) {
          ABind *elem = *ait;
          vect->push_back(elem);
        }  
        break;
    }
  }
}
