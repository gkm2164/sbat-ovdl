#include <string>

using namespace std;

#include <argument/sbat.argument.hh>
#include <argument/sbat.arg.string.hh>

namespace SBAT {
  ArgString::ArgString (string _value):
    Argument(arg_type::ARG_STRING, _value), value(_value) {}

  string
  ArgString::getValue () {
    return value;
  }
}
