#include <string>

using namespace std;

#include <argument/sbat.argument.hh>
#include <argument/sbat.arg.vname.hh>

namespace SBAT {
  ArgVName::ArgVName(string _name):
    Argument(arg_type::ARG_VNAME, _name), name(_name) {
  }

  string
  ArgVName::getName () {
    return name;
  }
} 
