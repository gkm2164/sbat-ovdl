#include <argument/sbat.argument.hh>
#include <argument/sbat.arg.vars.hh>


#include <sbat.symboltable.hh>
#include <sbat.symboltable.type.hh>

namespace SBAT {
  ArgVariableName::ArgVariableName (string _name):
    Argument(arg_type::ARG_VARIABLE, _name), name(_name) {
    var = (Variable *)current_stable->findByName(name, symbol_type::VARIABLE);

    if (var == NULL) {
      var = new Variable(name);
      current_stable->insertNew(name, symbol_type::VARIABLE, var);
    }
  }

  string ArgVariableName::getName() {
    return name;
  }
}
