#include <argument/sbat.argument.hh>
#include <argument/sbat.arg.number.hh>

namespace SBAT {
  ArgNumber::ArgNumber(int _value):
    Argument(arg_type::ARG_NUMBER, _value), value(_value) {}

  int
  ArgNumber::getValue() {
    return value;
  }
}
