#include <argument/sbat.argument.hh>
#include <argument/sbat.arg.reference.hh>

namespace SBAT {
  ArgReference::ArgReference(string _name):
    Argument(arg_type::ARG_REFERENCE, _name), name(_name) {
  }

  string
  ArgReference::getName() {
    return name;
  }
}
