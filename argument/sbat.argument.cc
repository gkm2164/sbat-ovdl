#include <string>

using namespace std;

#include <argument/sbat.argument.hh>

namespace SBAT {
  Argument::Argument (int _type): type(_type) {}
	Argument::Argument (int _type, int _ivalue):
		type(_type), ivalue(_ivalue) {}
	Argument::Argument (int _type, string _svalue):
		type(_type), svalue(_svalue) {}

  int
  Argument::getType() {
    return type;
  }
}
