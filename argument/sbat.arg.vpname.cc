#include <string>

using namespace std;

#include <argument/sbat.argument.hh>
#include <argument/sbat.arg.vpname.hh>

namespace SBAT {
  ArgVPName::ArgVPName(string _name):
    Argument(arg_type::ARG_VPNAME, _name), name(_name) {
  }

  string
  ArgVPName::getName() {
    return name;
  }
}
