#include <stdio.h>

#ifdef __HELLO__
extern f;
#endif

int main() {
  printf ("Hello World!\n");
#ifdef __HELLO__
  printf ("Hello 2");
  f();
#endif
  return 0;
}
