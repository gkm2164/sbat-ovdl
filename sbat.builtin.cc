#include <cstdlib>

#include <iostream>

#include <sbat.console.hh>
#include <sbat.builtin.hh>
#include <statement/sbat.varname.hh>
#include <sbat.symboltable.hh>

using namespace std;

namespace SBAT {

  extern SymbolTable *current_stable;

  void builtin_print(std::vector<Expression *> *arglist, void *ptr) {
    for (int i = 0; i < arglist->size(); i++) {
      cout << ((*arglist)[i])->eval().toString();
    }
    cout << endl;
  }

  void builtin_link(std::vector<Expression *> *arglist, void *ptr) {
    extern char **environ;
    
    VarNameExp *ve = (VarNameExp *)(*arglist)[0];
    string name = ve->getName();
    string refname1 = name + ".name";
    string refname2 = name + ".objs";
    
    Variable *bind = (Variable *)current_stable->findByName (refname1, symbol_type::STRING);
    Variable *var = (Variable *)current_stable->findByName (refname2, symbol_type::STRING);
    msg_prn (msg_type::INFO, msg_context::START, "Link " + var->getStrValue () + " to " + bind->getStrValue(), true);
    
    string command = "gcc -o " + bind->getStrValue() + " " + var->getStrValue();
    system (command.c_str ());
  }
}
