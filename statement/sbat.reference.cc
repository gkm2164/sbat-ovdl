#include <iostream>

#include <sbat.symboltable.hh>
#include <statement/sbat.number.hh>
#include <statement/sbat.string.hh>
#include <statement/sbat.reference.hh>

namespace SBAT {
  ReferenceExp::ReferenceExp(string _name):
    SymbolExp(SYM::REFERENCE), name(_name) {}

  Expression& ReferenceExp::eval () {
    string msg = "[Undefined reference to " + name + "]";

    switch (current_stable->getSymbolType (name)) {
      case symbol_type::STRING:
      {
        Variable *var = (Variable *)current_stable->findByName(name, symbol_type::STRING);
        return *(new StringExp(var->getStrValue()));
      }
      default: {
        current_stable->showElements();
        break;
      }
    }

    return *(new StringExp(msg));
  }

}
