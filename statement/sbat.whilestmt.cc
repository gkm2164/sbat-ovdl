#include <statement/sbat.whilestmt.hh>

namespace SBAT {
	WHILEStatement::WHILEStatement(Expression *e, StmtList *s):
		cond(e), stmts(s) {
	}
}
