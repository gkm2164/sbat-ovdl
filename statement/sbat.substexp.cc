#include <assert.h>

#include <iostream>
#include <string>

using namespace std;

#include <statement/sbat.expression.hh>
#include <statement/sbat.substexp.hh>

namespace SBAT {
  SubstExp::SubstExp(Expression *_x, Expression *_y):
    Expression('S'), x(_x), y(_y) {
  }

	Expression&
	SubstExp::eval() {
    Expression *_y = y;

    assert (x->getOPCode() == EXP::SYMBOL);
    if (y->getOPCode() != EXP::TERM) {
      _y = &y->eval();
    }

    assert (_y->getOPCode() == EXP::TERM);

    *x = *_y;
    res = x;
		return *x;
	}

  void
  SubstExp::doStmt() {
    eval();
  }
}
