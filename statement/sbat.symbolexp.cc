#include <statement/sbat.symbolexp.hh>

namespace SBAT {
	SymbolExp::SymbolExp(int _type):
		Expression(EXP::SYMBOL), type(_type) {
	}

	int
  SymbolExp::getType () { return type; }
}

