#include <statement/sbat.vpname.hh>

namespace SBAT {
  VPNameExp::VPNameExp(string _name):
    SymbolExp(SYM::VPNAME), name(_name) {
  }
}
