#include <assert.h>
#include <iostream>

#include <typeinfo>

using namespace std;

#include <statement/sbat.expression.hh>
#include <statement/sbat.binaryexp.hh>
#include <statement/sbat.number.hh>
#include <statement/sbat.string.hh>

#define BINOP(x, y, op) ((x) op (y))

namespace SBAT {
	BinaryExp::BinaryExp(int opcode, Expression *_x, Expression *_y):
		Expression (opcode), x(_x), y(_y) {
	}
  
  Expression&
  BinaryExp::eval() {
    Expression* __x = x;
    Expression* __y = y;

    // Terminate it anyway.
    if (x->getOPCode() != EXP::TERM) { 
    	__x = &x->eval();
    }
    if (y->getOPCode() != EXP::TERM) {
      __y = &y->eval();
    }

    assert (__x->getOPCode() == EXP::TERM &&
            __y->getOPCode() == EXP::TERM);
    
    Expression& _x = *__x;
    Expression& _y = *__y;
    
    switch (getOPCode()) {

      case compare::LT: res = &(_x < _y); break;
      case compare::GT: res = &(_x > _y); break;
      case compare::EQ: res = &(_x == _y); break;
      case compare::LE: res = &(_x <= _y); break;
      case compare::GE: res = &(_x >= _y); break;
      case compare::NE: res = &(_x != _y); break;
      case compare::AND: res = &(_x && _y); break;
      case compare::OR: res = &(_x || _y); break;
      case '+': res = &(_x + _y); break;
      case '-': res = &(_x - _y); break;
      case '*': res = &(_x * _y); break;
      case '/': res = &(_x / _y); break;
      default: break;
    }

    res = &res->eval();
    assert(res->getOPCode() == EXP::TERM);
    return *res;
	}

}
