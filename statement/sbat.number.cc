#include <iostream>
#include <string>
#include <sstream>

using namespace std; 
#include <statement/sbat.number.hh>

namespace SBAT {
  NumberExp::NumberExp(int _value):
		TermExp(TERM::NUMBER), value(_value) {}
  
  string NumberExp::toString() {
    stringstream ss;
    ss << value;
    return ss.str();
  }

  int NumberExp::toInteger() {
    return value;  
  }

  Expression& NumberExp::operator< (Expression &y) { 
    return *(new NumberExp(value < y.toInteger()));
  }

  Expression& NumberExp::operator> (Expression &y) {
    return *(new NumberExp(value < y.toInteger ()));
  }

  Expression& NumberExp::operator== (Expression &y) {
    return *(new NumberExp(value == y.toInteger()));
  }

  Expression& NumberExp::operator<= (Expression &y) {
    return *(new NumberExp(value <= y.toInteger()));
  }

  Expression& NumberExp::operator>= (Expression &y) {
    return *(new NumberExp(value >= y.toInteger()));
  }

  Expression& NumberExp::operator!= (Expression &y) {
    return *(new NumberExp(value != y.toInteger()));
  }

  Expression& NumberExp::operator&& (Expression &y) {
    return *(new NumberExp(value && y.toInteger()));
  }

  Expression& NumberExp::operator|| (Expression &y) {
    return *(new NumberExp(value || y.toInteger()));
  }

  Expression& NumberExp::operator+ (Expression &y) {
    return *(new NumberExp(value + y.toInteger()));
  }

  Expression& NumberExp::operator- (Expression &y) {
    return *(new NumberExp(value - y.toInteger()));
  }

  Expression& NumberExp::operator* (Expression &y) {
    return *(new NumberExp(value * y.toInteger()));
  }

  Expression& NumberExp::operator/ (Expression &y) {
    return *(new NumberExp(value / y.toInteger()));
  }

  Expression& NumberExp::eval() {
    return *this;
  }
}
