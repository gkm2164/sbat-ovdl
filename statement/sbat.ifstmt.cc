#include <assert.h>

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include <statement/sbat.ifstmt.hh>
#include <statement/sbat.termexp.hh>

namespace SBAT {
	IFStatement::IFStatement(Expression *e, std::vector<Statement *> *sts):
		compare(e), stmts(sts), elseifs(NULL), elsestmts(NULL) {
	}

	IFStatement::IFStatement(Expression *e, std::vector<Statement *> *sts,
		std::vector<Statement *> *elsest):
		compare(e), stmts(sts), elseifs(NULL), elsestmts(elsest) {
	}
	
	IFStatement::IFStatement(Expression *e, std::vector<Statement *> *sts,
		std::vector<IFStatement *> *elifs):
		compare(e), stmts(sts), elseifs(elifs), elsestmts(NULL) {
	}

	IFStatement::IFStatement(Expression *e, std::vector<Statement *> *sts,
		std::vector<IFStatement *> *elifs, std::vector<Statement *> *elsest):
		compare(e), stmts(sts), elseifs(elifs), elsestmts(elsest) {
	}

  bool
  IFStatement::test() {
    Expression& x = compare->eval();
		
		assert (x.getOPCode() == EXP::TERM);

		TermExp *__xt = (TermExp *)&x;
		return ((__xt->getType() == TERM::NUMBER && x.toInteger() != 0)
				|| (__xt->getType() == TERM::STRING && x.toString() != ""));
  }

  void
  IFStatement::doStmt () {
    assert (compare != NULL);

    if (test() == true) {
    	vector<Statement *>& slist = *stmts;
      for (int i = 0; i < slist.size(); i++) {
        slist[i]->doStmt();
      }
    } else if (elseifs != NULL) {
      for (int i = 0; i < elseifs->size(); i++) {
        IFStatement *eifs = (*elseifs)[i];
        if (eifs->test()) {
          eifs->doStmt();
          goto ifout;
        }
      }
    } else if (elsestmts != NULL) {
    	for (int i = 0; i < elsestmts->size(); i++) {
    		Statement *elem = (*elsestmts)[i];
        elem->doStmt();
      }
    }

ifout:
    int temp = 0;
  }
}
