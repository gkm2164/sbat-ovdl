#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

#include <sbat.symboltable.hh>
#include <statement/sbat.statement.hh>
#include <statement/sbat.shell.hh>
#include <utility/macro.hh>

extern char **environ;

namespace SBAT {
  SHELLStatement::SHELLStatement (string _shellName, string _cmds):
    shellName(_shellName), cmds(_cmds) {
    
  }

  MacroTable *mactbl;
  Macro *m;


  void
  runShell (string name, string cmds) {
    int pid = fork();
	  int fd = open ("temp.sh", O_CREAT | O_WRONLY, 0666);
 		
    mactbl = new MacroTable();

 		current_stable->toMacroTable(*mactbl);
    m = new Macro(mactbl, cmds);

    cmds = m->replaceString();

    write (fd, cmds.c_str(), cmds.size());
    close (fd);
    
    if (pid == 0) {
      int ret = 0;
      char *path = strdup(name.c_str());
      char *newargv[] = { NULL, (char *)"temp.sh", NULL };
  
      newargv[0] = path;
      ret = execve(path, newargv, environ);

      cout << "is failed..." << endl;
      exit (EXIT_FAILURE);
    }

    wait(NULL);
		unlink("temp.sh");

    delete mactbl;
    delete m;
  }

  void
  SHELLStatement::doStmt() {
    char *args[] = { NULL };
    char *envs[] = { NULL };
    cout << "Shell name: " << shellName << endl;
    
    current_stable->showElements ();

    runShell(shellName, cmds);
  }
}
