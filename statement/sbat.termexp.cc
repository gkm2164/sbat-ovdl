#include <string>

using namespace std;

#include <statement/sbat.expression.hh>
#include <statement/sbat.termexp.hh>

namespace SBAT {
	
	TermExp::TermExp(int opcode):
		Expression(EXP::TERM), termID(opcode) {
  }

}
