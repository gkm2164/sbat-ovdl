#include <iostream>
#include <vector>

using namespace std;

#include <statement/sbat.expression.hh>
#include <statement/sbat.funccallexp.hh>
#include <statement/sbat.termexp.hh>
#include <statement/sbat.varname.hh>
#include <statement/sbat.functable.hh>


namespace SBAT {
	FuncCallExp::FuncCallExp(SymbolExp *_ref, std::vector<Expression *> *_arglist)
		: Expression('F'), ref(_ref), arglist(_arglist) {

	}


	Expression&
	FuncCallExp::eval() {
		if (ref->getType() == SYM::VARIABLE) { /* built-in? */
			VarNameExp *vne = (VarNameExp *)ref;
      fcall func = builtin_func.getFunction(vne->getName());
      if(func) {
        func(arglist, NULL);
      }
		}
		return *(new TermExp(0));
	}
}
