#include <statement/sbat.forstmt.hh>

namespace SBAT {
	FORStatement::FORStatement(Expression *_first, Expression *_cond, Expression *_after, StmtList *_stmts):
		first(_first), cond(_cond), after(_after), stmts(_stmts) {
	}
}
