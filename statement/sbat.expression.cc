#include <assert.h>
#include <iostream>
#include <string>
#include <vector>
#include <typeinfo>

using namespace std;

#include <statement/sbat.expression.hh>
#include <statement/sbat.number.hh>
#include <statement/sbat.string.hh>
#include <statement/sbat.termexp.hh>

namespace SBAT {
	Expression::Expression (int _opcode):
		opcode(_opcode) { }

	int
	Expression::getOPCode() { return opcode; }
 
  Expression&
  Expression::eval() {
    if (res != NULL) {
      if (res->getOPCode() != EXP::TERM) {
        res = &res->eval();
      }

      assert(res->getOPCode() == EXP::TERM);
      return *res;
    }

    return *(new NumberExp(0));
  }

  void
  Expression::doStmt() {
    if (res || res->getOPCode() != EXP::TERM) {
      res = &res->eval();
    }
  }

  Expression& Expression::operator= (Expression &x) {
  	if (x.getOPCode() != EXP::TERM) {
  		Expression *__x = &(x.eval());

      assert(__x->getOPCode() == EXP::TERM);

      TermExp *val = (TermExp *)__x;

      if (val->getType() == TERM::NUMBER) {
      	res = new NumberExp(val->toInteger());

      } else if (val->getType() == TERM::STRING) {
     		res = new StringExp(val->toString());
      }
    }

   	return *res;
  }
}
