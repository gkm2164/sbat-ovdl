#include <statement/sbat.functable.hh>

namespace SBAT {
  FunctionTable::FunctionTable () {
  
  }

  void FunctionTable::registFunc (string name, fcall func) {
    funcmap.insert(pair<string, fcall>(name, func));
  }

  fcall
  FunctionTable::getFunction(string name) {
    if (funcmap.find(name) == funcmap.end()) {
      return NULL;
    }
    return funcmap[name];
  }

  FunctionTable builtin_func;
}
