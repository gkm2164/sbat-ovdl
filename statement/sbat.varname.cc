#include <assert.h>

#include <iostream>

#include <statement/sbat.number.hh>
#include <statement/sbat.string.hh>
#include <statement/sbat.varname.hh>

#include <sbat.symboltable.hh>

namespace SBAT {
  VarNameExp::VarNameExp(string _name):
    SymbolExp(SYM::VARIABLE), name(_name) {
    var = (Variable *)current_stable->findByName(name, symbol_type::VARIABLE);
    if (var == NULL) {
      current_stable->insertNew(name, SYM::VARIABLE, new Variable(name));
      var = (Variable *)current_stable->findByName(name, symbol_type::VARIABLE);
    }
  }
  
  void
  VarNameExp::setExpectType(int type) {
    expectType = type;
  }
  
  Expression&
  VarNameExp::eval() {
    assert ((var->getType() ^ (VARTYPE::STRING | VARTYPE::INTEGER)) != 0);
    cout << "Type of variable for " << name << ": " << var->getType() << endl;
    if (var->getType() == VARTYPE::STRING) {

      return *(new StringExp(var->getStrValue()));

    } else {

      return *(new NumberExp(var->getIntValue()));

    }
  }

  Expression&
  VarNameExp::operator=(Expression &x) {
    Expression *_x = &x;

    if (x.getOPCode() != EXP::TERM) {
      _x = &(x.eval());
      assert(_x->getOPCode() == EXP::TERM);
    }

    TermExp *__x = (TermExp *)_x;

    if (__x->getType() == TERM::NUMBER) {
      var->setValue(__x->toInteger());
    }
    else if(__x->getType() == TERM::STRING) {
      var->setValue(__x->toString());
    }
  }
}
