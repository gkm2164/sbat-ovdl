#include <statement/sbat.expression.hh>
#include <statement/sbat.unaryexp.hh>
#include <statement/sbat.number.hh>

namespace SBAT {
	UnaryExp::UnaryExp (int opcode, Expression *_x):
		Expression(opcode), x(_x) {
	}

	Expression&
	UnaryExp::eval () {
		if (x->getOPCode() != EXP::TERM) {
			x->eval();
		}
		
		NumberExp *ret = NULL;

		switch (getOPCode()) {
			case 'N':
				return !(*x);
				break;
			case 'M':
				return !(*x);
				break;
		}
		return *(new NumberExp(0));
	}
}
