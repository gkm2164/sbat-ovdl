#include <statement/sbat.string.hh>
#include <statement/sbat.number.hh>

namespace SBAT {
  StringExp::StringExp(string _value):
    TermExp(TERM::STRING), value(_value) {}
    
  Expression& StringExp::operator< (Expression &x) {
    return *(new NumberExp(value < x.toString()));
  }

  Expression& StringExp::operator> (Expression &x) {
    return *(new NumberExp(value > x.toString()));
  }

  Expression& StringExp::operator== (Expression &x) {
    return *(new NumberExp(value == x.toString()));
  }

  Expression& StringExp::operator<= (Expression &x) {
    return *(new NumberExp(value <= x.toString()));
  }

  Expression& StringExp::operator>= (Expression &x) {
    return *(new NumberExp(value >= x.toString()));
  }

  Expression& StringExp::operator!= (Expression &x) {
    return *(new NumberExp(value != x.toString()));
  }

  Expression& StringExp::operator&& (Expression &x) {
    return *(new NumberExp(value != "" && x.toString() != ""));
  }

  Expression& StringExp::operator|| (Expression &x) {
    return *(new NumberExp(value != "" || x.toString() != ""));
  }

  Expression& StringExp::operator+ (Expression &x) {
    return *(new StringExp(value + x.toString()));
  }

  Expression& StringExp::operator- (Expression &x) {
    return *(new StringExp(""));
  }

  Expression& StringExp::operator* (Expression &x) {
    return *(new StringExp(""));
  }

  Expression& StringExp::operator/ (Expression &x) {
    return *(new StringExp(""));
  }
}
