#include <iostream>

#include <statement/sbat.vname.hh>
#include <statement/sbat.string.hh>

namespace SBAT {
  VNameExp::VNameExp(string _name):
    SymbolExp(SYM::VNAME), name(_name) {
  }

  Expression &VNameExp::eval () {
    return *(new StringExp(name));
  }
}
