#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

#include <sbat.console.hh>
#include <sbat.binding.hh>

#include <sbat.symboltable.hh>
#include <sbat.symboltable.type.hh>

typedef SBAT::symbol_type stype;

namespace SBAT {
	Binding::Binding(string _prodName, vector<ABind *> *_blist):
		productName(_prodName), bindlist(_blist) {

	}

	void
	Binding::expandBinding(Platform *pf) {
		vector<string> *ret = new vector<string> ();
    def_str = "";


		vector<string>::const_iterator pit = pf->getIterator();
		while (pit != pf->getEnd()) {
			string str = *pit;
		  ret->push_back(str);

			if (current_stable->getSymbolType(str) == stype::VARIABLE_POINT) {
        if (bindlist != NULL) {
          vector<ABind *>::const_iterator bit = bindlist->begin();
          while (bit != bindlist->end()) {
            if (str == (*bit)->first) {
              ret->push_back(str + "." + (*bit)->second);
              def_str += " -D" + str.substr(2) + "_" + (*bit)->second.substr(1);
            }
            bit++;
          }
        }
			}
			
			pit++;
		}

		this->binds = ret;
		setBindingCodes();

    msg_prn(msg_type::DEBUG, msg_context::START, "Binding name: " + productName + " (" + (*ret)[0], false);
    for (int i = 1; i < ret->size(); i++) {
      msg_prn(msg_type::DEBUG, msg_context::MIDDLE, ", " + (*ret)[i], false);
    }
    msg_prn(msg_type::DEBUG, msg_context::MIDDLE, ")", true);
    init_vars (productName);
	}

  vector<ABind *> *
  Binding::getBindList() {
    return bindlist;
  }

  vector<string> *
  Binding::getBinds() {
    return binds;
  }

  void
  Binding::setBindingCodes() {
  	codes = new map<string, string>();
  	for (int i = 0; i < binds->size(); i++) {
  		string elem = (*binds)[i];
  		if (current_stable->getSymbolType(elem) == symbol_type::VARIANT) {
  			Variant *v = (Variant *)current_stable->findByName(elem, symbol_type::VARIANT);
  			if (v->getSourceName () == "") {
					v->setSourceName(v->getName().substr(1, v->getName().size() - 1));
  			}
  			codes->insert(pair<string, string>(elem, v->getSourceName()));
  		}
  	}
  }

  string
  Binding::expandSources (string refs) {
    bool first = true;
    string ret = "";
    vector<string>::const_iterator it = binds->begin();

    for (; it != binds->end(); it++) {
      string v_name = *it;
      string refname = v_name + refs;
      Variable *v = (Variable *)current_stable->findByName (refname, symbol_type::STRING);
      if (v == NULL)
        continue;
      if (first) {
        ret += v->getStrValue();
        first = false;
      } else {
        ret += " " + v->getStrValue();
      }
    }

    return ret;
  }

  void
  Binding::init_vars(string init_name) {
    var_name = new Variable (init_name + ".name");
    var_name->setValue (productName);
    var_def_str = new Variable (init_name + ".defstr");
    var_def_str->setValue (def_str);
    bindings = new Variable (init_name + ".bindings");
    bindings->setValue (expandSources(".name"));
    srcs = new Variable (init_name + ".srcs");
    srcs->setValue (expandSources(".src"));
    objs = new Variable (init_name + ".objs");
    objs->setValue (expandSources(".obj"));

    current_stable->removeIfExist(init_name + ".name");
    current_stable->removeIfExist(init_name + ".defstr");
    current_stable->removeIfExist(init_name + ".bindings");
    current_stable->removeIfExist(init_name + ".srcs");
    current_stable->removeIfExist(init_name + ".objs");

    current_stable->insertNew(init_name + ".name", symbol_type::STRING, var_name);
    current_stable->insertNew(init_name + ".defstr", symbol_type::STRING, var_def_str);
    current_stable->insertNew(init_name + ".bindings", symbol_type::STRING, bindings);
    current_stable->insertNew(init_name + ".srcs", symbol_type::STRING, srcs);
    current_stable->insertNew(init_name + ".objs", symbol_type::STRING, objs);
  }
  
  void
  Binding::_builtin_compile(std::vector<Expression *> *arglist, void *aux) {
    Binding *ptr = (Binding *)aux;

    ptr->builtin_compile(arglist);
  }

  void
  Binding::builtin_compile(std::vector<Expression *> *arglist) {
    
  }
}
